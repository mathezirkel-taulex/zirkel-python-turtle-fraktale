from turtle import *

import turtle
from math import sqrt, cos, sin, pi

def sierpinski(turtle, laenge, schritte, winkel):
  turtle.pendown()
  
  if schritte < 1:
    dreieckMitRechteckDeckend(turtle, laenge, winkel)
  
  else:
    sierpinski(turtle, laenge/2, schritte-1, winkel)
    turtle.forward(laenge/2)
    
    sierpinski(turtle, laenge/2, schritte-1, winkel)
    turtle.forward(laenge/2)
    turtle.left(120)
    turtle.forward(laenge/2)
    turtle.left(60)
    turtle.forward(laenge/2)
    turtle.right(180)
  
    sierpinski(turtle, laenge/2, schritte-1, winkel)
    turtle.right(120)
    turtle.forward(laenge/2)
    turtle.left(120)
  
  turtle.penup()


def dreieckMitRechteckDeckend(turtle, laenge, winkel):
  turtle.seth(0)
  rechteckLaenge = (laenge / 2) / cos((winkel / 2) * ((2 * pi) / 360))
  a = rechteckLaenge
  
  #Dreieck machen:
  turtle.fillcolor("yellow")
  turtle.begin_fill()
  turtle.penup()
  turtle.forward(laenge)
  turtle.left(120)
  turtle.pendown()
  turtle.forward(laenge)
  dreieckObenX = turtle.xcor()
  dreieckObenY = turtle.ycor()
  turtle.left(120)
  turtle.forward(laenge)
  turtle.left(120)
  turtle.end_fill()
  
  turtle.seth(0)
  
  #Viereck machen
  turtle.right(winkel/2)
  turtle.fillcolor("orange")
  turtle.begin_fill()
  turtle.forward(a)
  turtle.left(winkel)
  turtle.forward(a)
  turtle.left(180 - winkel)
  turtle.forward(a)
  viereckObenX = turtle.xcor()
  viereckObenY = turtle.ycor()
  turtle.left(winkel)
  turtle.forward(a)
  turtle.left(180 - winkel)
  turtle.end_fill()
  
  turtle.seth(0)
  
  #noch den Strich vom oberen Rechteckeck hoch zum Dreieckeck
  turtle.penup()
  turtle.goto(viereckObenX, viereckObenY)
  turtle.color("black")
  turtle.pendown()
  turtle.goto(dreieckObenX, dreieckObenY)
  turtle.penup()
  turtle.seth(-120)
  turtle.forward(laenge)
  turtle.seth(0)
  

inge = Turtle()
inge.shape("turtle")
inge.speed(0)

inge.penup()
inge.goto(-100, -80)

num = 3
sierpinski(inge, 200, num, 50)
inge.hideturtle()


canv = inge.getscreen().getcanvas() 
canv.postscript(file="/home/bimmler/Desktop/turtleexports/sierpinski-" + str(num) + "-rautePerspektivisch.ps") 
