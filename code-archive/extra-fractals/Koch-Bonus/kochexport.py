from turtle import *
import random


def kochFlocke(turtle, laenge, limitmin):
  turtle.pendown()

  for i in range(3):
    kochZacke(turtle, laenge, limitmin)
    turtle.right(120)

  turtle.penup()


def kochKurve(turtle, laenge, limitmin):
  kochZacke(turtle, laenge, limitmin)
  turtle.left(60)
  kochZacke(turtle, laenge, limitmin)
  turtle.right(120)
  kochZacke(turtle, laenge, limitmin)
  turtle.left(60)
  kochZacke(turtle, laenge, limitmin)


def kochZacke(turtle, laenge, limitmin):
  if laenge > limitmin*3:
    kochKurve(turtle, laenge / 3, limitmin)
  else:
    a = random.randint(1, 255) /255
    b = random.randint(1, 255)/255
    c = random.randint(1, 255)/255
    turtle.color((a, b, c))
    turtle.forward(laenge)


inge = Turtle()
inge.shape("turtle")
inge.speed(25)
inge.pensize(2)

inge.penup()
inge.goto(-100, 100)

kochFlocke(inge, 200, 200)

inge.hideturtle()

canv = inge.getscreen().getcanvas() 
canv.postscript(file="/home/bimmler/Desktop/turtleexports/koch-bunt1it.ps") 
