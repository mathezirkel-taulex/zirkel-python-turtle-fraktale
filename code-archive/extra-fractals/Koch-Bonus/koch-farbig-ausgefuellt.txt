main.py:

from turtle import *
from shapes import *

inge = Turtle()
inge.shape("turtle")
inge.speed(25)

inge.color("blue")
inge.fillcolor("sky blue")

inge.penup()
inge.goto(-100, 100)

inge.begin_fill()
kochFlocke(inge, 200, 5)
inge.end_fill()

inge.hideturtle()




shapes.py:

import turtle

def kochFlocke(turtle, laenge, limitmin):
  turtle.pendown()

  for i in range(3):
    kochZacke(turtle, laenge, limitmin)
    turtle.right(120)

  turtle.penup()


def kochKurve(turtle, laenge, limitmin):
  kochZacke(turtle, laenge, limitmin)
  turtle.left(60)
  kochZacke(turtle, laenge, limitmin)
  turtle.right(120)
  kochZacke(turtle, laenge, limitmin)
  turtle.left(60)
  kochZacke(turtle, laenge, limitmin)


def kochZacke(turtle, laenge, limitmin):
  if laenge > limitmin*3:
    kochKurve(turtle, laenge / 3, limitmin)
  else:
    turtle.forward(laenge)
