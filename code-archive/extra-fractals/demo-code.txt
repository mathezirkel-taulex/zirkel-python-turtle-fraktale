main.py:






from turtle import *
from shapes import *
import time
import random

inge = Turtle()
inge.shape("turtle")
inge.speed(0)
#inge.tracer(8, 25)

sleeptime = 2

while True:

  #hier flocke
  
  inge.color("black")
  inge.fillcolor("blue")
  
  inge.penup()
  inge.goto(-100, 100)
  
  it = random.randint(0, 3)
  
  kochFlocke(inge, 200, it)
  inge.goto(0, -100)
  inge.write("Meine " + str(it) + "-Kochsche Schneeflocke :-)", None, "center", "15pt bold")
  inge.goto(0, -115)
  inge.seth(0)
  time.sleep(sleeptime)
  inge.clear()
  
  #hier Menger-Schwamm
  inge.color("black")
  inge.fillcolor("grey")
  
  it = random.randint(0, 2)
  
  inge.penup()
  inge.goto(-100, 130)
  menger(inge, 200, it)
  inge.goto(0, -100)
  inge.write("Mein " + str(it) + "-Menger-Schwamm :-)", None, "center", "15pt bold")
  inge.goto(0, -115)
  inge.seth(0)
  time.sleep(sleeptime)
  inge.clear()
  
  #hier Sierpinski-Dreieck
  
  inge.color("black")
  inge.fillcolor("orange")

  inge.penup()
  inge.goto(-100, -80)
  
  it = random.randint(0, 3)

  sierpinski(inge, 200, it)

  inge.goto(0, -110)
  inge.write("Mein " + str(it) + "-Sierpinski-Dreieck :-)", None, "center", "15pt bold")
  inge.goto(0, -135)
  inge.seth(0)
  
  time.sleep(sleeptime)
  inge.clear()
  








shapes.py:





import turtle

def kochFlocke(turtle, laenge, schritte):
  turtle.pendown()
  turtle.begin_fill()
  
  for i in range(3):
    kochZacke(turtle, laenge, schritte)
    turtle.right(120)
    
  turtle.end_fill()
  turtle.penup()


def kochKurve(turtle, laenge, schritte):
  kochZacke(turtle, laenge, schritte)
  turtle.left(60)
  kochZacke(turtle, laenge, schritte)
  turtle.right(120)
  kochZacke(turtle, laenge, schritte)
  turtle.left(60)
  kochZacke(turtle, laenge, schritte)


def kochZacke(turtle, laenge, schritte):
  if schritte > 1:
    kochKurve(turtle, laenge / 3, schritte-1)
  else:
    turtle.forward(laenge)


#fürn Menger Schwamm

import turtle

def menger(turtle, laenge, schritte):
  turtle.pendown()
  
  if schritte < 1:
    quadrat(turtle, laenge)

  else:
    menger(turtle, laenge/3, schritte-1)
    turtle.forward(laenge/3)
  
    menger(turtle, laenge/3, schritte-1)
    turtle.forward(laenge/3)
  
    menger(turtle, laenge/3, schritte-1)
    turtle.forward(laenge/3)
    turtle.right(90)
    turtle.forward(laenge/3)
  
    menger(turtle, laenge/3, schritte-1)
    turtle.forward(laenge/3)
  
    menger(turtle, laenge/3, schritte-1)
    turtle.forward(laenge/3)
    turtle.right(90)
    turtle.forward(laenge/3)
  
    menger(turtle, laenge/3, schritte-1)
    turtle.forward(laenge/3)
  
    menger(turtle, laenge/3, schritte-1)
    turtle.forward(laenge/3)
    turtle.right(90)
    turtle.forward(laenge/3)
  
    menger(turtle, laenge/3, schritte-1)
    turtle.forward(laenge/3)
    turtle.forward(laenge/3)
    turtle.right(90)
  
  turtle.penup()



def quadrat(turtle, laenge):
  turtle.begin_fill()
  for i in range(4):
    turtle.forward(laenge)
    turtle.right(90)
  turtle.end_fill()  


#hier sierpinski:

def sierpinski(turtle, laenge, schritte):
  turtle.pendown()
  
  if schritte < 1:
    dreieck(turtle, laenge)
  
  else:
    sierpinski(turtle, laenge/2, schritte-1)
    turtle.forward(laenge/2)
    
    sierpinski(turtle, laenge/2, schritte-1)
    turtle.forward(laenge/2)
    turtle.left(120)
    turtle.forward(laenge/2)
  
    sierpinski(turtle, laenge/2, schritte-1)
    turtle.forward(laenge/2)
    turtle.left(120)
    turtle.forward(laenge/2)
  
    turtle.forward(laenge/2)
    turtle.left(120)
  
  turtle.penup()

def dreieck(turtle, laenge):
  turtle.begin_fill()
  for i in range(3):
    turtle.forward(laenge)
    turtle.left(120)
  turtle.end_fill()

