import math
import random
import matplotlib.pyplot as plt

#scale for the plot, for a standard pc monitor 100-1000 seems fine
scale = 100

#everywhere k is the last corner i went to and n the amount of corners
numberofrules = 4

#geht zu einem links, einem rechts, oder dem letzten selbst
def rule1(k, n):
    new = (k + random.randint(-1,1))%n
    return new

#zu einem in der linken hälfte vom letzen aus
def rule2(k, n):
    new = (k - random.randint(0, math.floor(n/2)))%n
    return new

#entweder zu einem links oder rechts vom letzten
def rule3(k, n):
    new = (k + (-1)**random.randint(0,1))%n
    return new

#darf zu allen außer dem letzen
def rule4(k, n):
    new = (k + random.randint(1, n - 1))%n
    return new

#usability feature 'ignore'
def arbitrule(i, n, k):
    if i == 1:
        return rule1(n, k)
    if i == 2:
        return rule2(n, k)
    if i == 3:
        return rule3(n, k)
    if i == 4:
        return rule4(n, k)
    else:
        print("there is no such rule!")
        arbitrule(int(input("please enter a valid rule: ")), n, k)


#takes a two points and outputs the point in the middle of them
def half_distance(P, Q):
    PQ = [(Q[0]-P[0])/2, (Q[1]-P[1])/2]
    newpoint = [(P[0]+PQ[0]), (P[1]+PQ[1])]
    return newpoint

#makes corners of a regular n-gon with sidelengths one in the first 2 quadrants with one corner at (0, 0), corners are labelled counterclockwise
def makecorners(n):
    Corners = []
    Corners.append([[0, 0],0])
    for k in range(1,n):
        Corners.append([[Corners[-1][0][0] + scale*math.cos(2*(math.pi)*k/n - math.pi/2), Corners[-1][0][1] + scale*math.sin(2*(math.pi)*k/n - math.pi/2)], k])
    return Corners

#takes a given point and iterates it to one of the corners
def iterate(Point, Corners, rlast=0, rule=1):
    r = arbitrule(rule, rlast, len(Corners))
    iteratedpoint = half_distance(Point, Corners[r][0])
    return [iteratedpoint, r]

#takes a given point and iterates it n times randomly
def iteratetill(n, StartPoint, Corners, rule):
    Pointlist = [[StartPoint, 0]]
    for k in range(0,n):
        Pointlist.append(iterate(Pointlist[-1][0], Corners, Pointlist[-1][1], rule))
    return Pointlist

#array's a listdictionary
def arrayup(List_of_Points_with_rs):
    array = [[], []]
    for n in range(0,len(List_of_Points_with_rs)):
        array[0].append(List_of_Points_with_rs[n][0][0])
        array[1].append(List_of_Points_with_rs[n][0][1])
    return array

#program graphing the strange attractors:
#location of the first point
Px = 0
Py = 0

#actual program
nraw = input("how many corners would you like your polygon to have? \n")
n = int(nraw)
print("which rule do you want to use? (available are: 1 to ", numberofrules, ")")
rulenumberraw =  input()
rulenumber = int(rulenumberraw)
kraw = input("how many iterations would you like? \n ")
k = int(kraw)
sos = input("would you like the plot to show(s) or to save(r)? \n")
array = arrayup(iteratetill(k, [Px, Py], makecorners(n), rulenumber))
cornerarray = arrayup(makecorners(n))
plt.plot(array[0], array[1], 'r,')
plt.plot(cornerarray[0], cornerarray[1], 'bo')
if sos == "s":
    plt.show()
else:
    if sos == "r":
        plt.savefig("fig.png", dpi='figure')
    else:
        print("no clue what you ment with", sos, ",so we just saved the plot")

#end
'''some suggestions for nice results:
    triangle with rule 1, 10000 iterations-> sierpinski
    square with rule 2, 10000 iterations -> weird lightning-thingy
    '''
