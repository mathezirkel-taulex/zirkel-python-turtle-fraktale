from turtle import *

def createTurtle(shape, speed, color):
  t = Turtle()
  t.shape(shape)
  t.speed(speed)
  t.color(color)
  t.penup()
  return t