from turtle import *
import helpers
import random

# create turtles in one line using the new helper function
#t1 = helpers.createTurtle("turtle", 7, "red")
#t2 = helpers.createTurtle("circle", 7, "green")

# colors and shapes list for randomizing
colors = ["red", "green", "blue", "cyan", "magenta", "orange", "yellow"]
shapes = ["classic", "arrow", "circle", "square", "triangle", "turtle"]
def getRandomColor():
  return colors[random.randint(0, len(colors)-1)]
def getRandomShape():
  return shapes[random.randint(0, len(shapes)-1)]

speed = 7

# test creating random turtles:
#t1 = helpers.createTurtle(getRandomShape(), speed, getRandomColor())
#t2 = helpers.createTurtle(getRandomShape(), speed, getRandomColor())

# create many random turtles:
turtles = []
for i in range(15):
  turtles.append(helpers.createTurtle(getRandomShape(), speed, getRandomColor()))

# mit list comprehension, aber eher unnötig, das so zu machen:
#turtles = [helpers.createTurtle(getRandomShape(), speed, getRandomColor()) for i in range(10)]

# distribute turtles on the canvas, make them look in random directions
canvasDim = 150
for t in turtles:
  t.goto(random.randint(-canvasDim, canvasDim), random.randint(-canvasDim, canvasDim))
  t.seth(random.randint(0, 360))

# try to naively make the turtles walk forward, will probably be bad bc of synchronous walking
for i in range(50):
  for t in turtles:
    t.forward(2)

