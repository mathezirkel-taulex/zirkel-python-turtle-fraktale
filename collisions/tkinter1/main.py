from tkinter import *
import math

# mit viel Inspiration von https://www.youtube.com/watch?v=IQWfaksVJC4

# tkinter canvas erstellen
width, height = 1000, 800
tk = Tk()
canvas = Canvas(tk, width=width, height=height)
canvas.pack()

# Ball class für mehrfache Nutzung erstellen
class Ball:
  def __init__(self, size, color, speed, direction):
    self.ball = canvas.create_oval(0, 0, size, size, fill=color)
    self.speed_x = speed * math.cos(direction)
    self.speed_y = speed * math.sin(direction)
    self.movement()

  def movement(self):
    canvas.move(self.ball, self.speed_x, self.speed_y)
    pos = canvas.coords(self.ball) # contains: 0) bottom left corner of ball x coordinate, 1) bottom left corner of ball y coordinate, 2) top right corner of ball x coordinate, 3) top right corner of ball y coordinate, 
    if pos[0] < 0 or pos[2] > width:
      self.speed_x *= -1
    if pos[1] < 0 or pos[3] > height:
      self.speed_y *= -1
    tk.after(20, self.movement) # nach 20 milliseconds wird movement wieder aufgerufen, d.h. wir haben eine Bewegung mit ca 50 Bildern pro Sekunde (zumindest berechnet, kommt dann noch auf den Bildschirm an)

# Bälle erstellen
ball1 = Ball(100, "red", 15, 70)
ball1 = Ball(10, "orange", 5, 10)
ball1 = Ball(50, "green", 40, -90)
ball1 = Ball(150, "black", 10, 50)

# tkinter loop starten
tk.mainloop()