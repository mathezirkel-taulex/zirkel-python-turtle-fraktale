from tkinter import *
import math
import random
from enum import Enum


class Mode(Enum):
  COLLISIONS = 1
  RPS_COLLISIONS = 2
  RPS_WOBBLE = 3

# mode = Mode.COLLISIONS
# mode = Mode.RPS_COLLISIONS
mode = Mode.RPS_WOBBLE

collisionHandling = mode in [Mode.COLLISIONS, Mode.RPS_COLLISIONS]
rpsHandling = mode in [Mode.RPS_COLLISIONS, Mode.RPS_WOBBLE]

amount = 35
standardSize = 30
standardSpeed = 100
noiseAmplifier = 2

# tkinter canvas erstellen
width, height = 500, 300
tk = Tk()
canvas = Canvas(tk, width=width, height=height)
canvas.pack()


class RPS(Enum):
  Rock = 3
  Paper = 2
  Scissors = 1

rpsArray = [RPS.Rock, RPS.Paper, RPS.Scissors]

def getRpsColor(rps: RPS):
  if rps == RPS.Rock: return "red"
  elif rps == RPS.Paper: return "blue"
  elif rps == RPS.Scissors: return "green"
  else: return "black"

def getRpsNumber(rps: RPS):
  if rps == RPS.Rock: return 1
  elif rps == RPS.Paper: return 2
  elif rps == RPS.Scissors: return 3
  else: return 0

def getRPSWinnerIsFirst(first: RPS, second: RPS):
  result = (getRpsNumber(first) - getRpsNumber(second) + 3) % 3
  if result == 0:
    return None
  if result == 1:
    return True
  if result == 2:
    return False

def getRPSWinnerBall(ball, other):
  result = getRPSWinnerIsFirst(ball.rps, other.rps)
  if result is None:
    return None
  if result is True:
    return ball
  if result is False:
    return other

def getRPSBallsAll(ball, allBalls):
  same, prey, avoid = [], [], []
  for other in allBalls:
    result = getRPSWinnerIsFirst(ball.rps, other.rps)
    if result is None:
      same.append(other)
    elif result is True:
      prey.append(other)
    elif result is False:
      avoid.append(other)
  return (same, prey, avoid)


def getDistanceBetweenTwoBalls(ball1, ball2):
  # für die folgenden Berechnungen nehmen wir an, dass y nach oben wächst, nicht nach unten. Genau wie bei allen anderen Stellen, an denen es um Winkel geht, also wie in einem normalen Koordinatensystem mit mathematisch positivem Drehsinn (gegen Uhrzeigersinn)
  distCentersX = ball2.center[0] - ball1.center[0]
  distCentersY = ball1.center[1] - ball2.center[1]
  distCenters = math.sqrt(distCentersX**2 + distCentersY**2)
  distBalls = distCenters - (ball1.radius + ball2.radius)
  return distBalls

def getAngleBetweenTwoBalls(ball1, ball2):
  # für die folgenden Berechnungen nehmen wir an, dass y nach oben wächst, nicht nach unten. Genau wie bei allen anderen Stellen, an denen es um Winkel geht, also wie in einem normalen Koordinatensystem mit mathematisch positivem Drehsinn (gegen Uhrzeigersinn)
  distCentersX = ball2.center[0] - ball1.center[0]
  distCentersY = ball1.center[1] - ball2.center[1]
  if distCentersX == 0 and distCentersY == 0:
    return 0
  return angleOfVectorInRadians(distCentersX, distCentersY)

def getDistanceAndAngleBetweenTwoBalls(ball1, ball2):
  # für die folgenden Berechnungen nehmen wir an, dass y nach oben wächst, nicht nach unten. Genau wie bei allen anderen Stellen, an denen es um Winkel geht, also wie in einem normalen Koordinatensystem mit mathematisch positivem Drehsinn (gegen Uhrzeigersinn)
  distCentersX = ball2.center[0] - ball1.center[0]
  distCentersY = ball1.center[1] - ball2.center[1]
  distCenters = math.sqrt(distCentersX**2 + distCentersY**2)
  distBalls = distCenters - (ball1.radius + ball2.radius)

  if distCenters == 0:
    return (0, 0)

  angleFromBallOneToBallTwo = angleOfVectorInRadians(distCentersX, distCentersY)
  
  return (distBalls, angleFromBallOneToBallTwo)


def angleOfVectorInRadians(x, y):
  if x == 0:
    return math.pi if y >= 0 else 3 * math.pi
  arctan = math.atan(y / x)
  angle = arctan + (math.pi if x < 0 else 0)
  return angle


def degToRadians(degrees):
  return (degrees / 360) * 2 * math.pi
def radToDegrees(radians):
  return (radians / (2 * math.pi)) * 360


# Ball class für mehrfache Nutzung erstellen
class Ball:
  def __init__(self, startX, startY, size, color, speed, direction, rps: RPS, allBalls):
    # init Parameter speichern für spätere Nutzung außerhalb von __init__
    self.size = size
    self.color = color
    self.speed = speed / 100
    self.direction = direction # im Bogenmaß, nicht in Grad!
    self.allBalls = allBalls

    self.radius = size / 2

    if rps is not None:
      self.updateRps(rps)
    
    # Objekt auf dem Canvas erstellen
    self.shape = canvas.create_oval(startX + 0, startY + 0, startX + size, startY + size, fill=self.color)
    
    # Geschwindigkeit aus speed und direction berechnen, canvas.move() braucht die Geschwindigkeit in x- und y-Richtung
    self.speedX = speed * math.cos(direction)
    self.speedY = speed * math.sin(direction)

    # direkt nach Erstellung des Balls rufen wir movement auf, um die Bewegung zu starten
    self.movement()
  
  def updateRps(self, newRps: RPS):
    self.rps = newRps
    self.color = getRpsColor(self.rps)
    if hasattr(self, "shape"):
      self.updateColorFill()
  
  def updateColorFill(self):
    canvas.itemconfig(self.shape, fill=self.color)
  
  # in einem Schwung die aktuelle Position und die variablen x1, y1, x2, y2 setzen, für leichter lesbaren Code
  def updatePos(self):
    self.pos = canvas.coords(self.shape) # wie schon die Dimensionen des Canvas, sind auch die Dimensionen aller anderen Objekte angegeben durch die obere linke Ecke und die untere rechte Ecke, davon jeweils die x- und y-Koordinate. Enthalten in canvas.coords(...) sind vier Zahlen: 0) x-Koordinate Ecke oben links 1) y-Koordinate Ecke oben links, 2) x-Koordinate Ecke unten rechts, 3) y-Koordinate Ecke unten rechts.
    self.x1 = self.pos[0]
    self.y1 = self.pos[1]
    self.x2 = self.pos[2]
    self.y2 = self.pos[3]
    self.center = [self.x1 + self.radius, self.y1 + self.radius]

  def collisionHandling(self):
    self.updatePos()

    # mit Wand kollidieren:
    x = self.x1
    y = self.y1
    tolerance = 10
    resolve = False
    if self.x1 < 0 - tolerance:
      resolve = True
      x = 0
      if collisionHandling:
        self.speedX = abs(self.speedX)
    elif self.x2 > width + tolerance:
      resolve = True
      x = width - self.size
      if collisionHandling:
        self.speedX = - abs(self.speedX)
    if self.y1 < 0 - tolerance:
      resolve = True
      y = 0
      if collisionHandling:
        self.speedY = abs(self.speedY)
    elif self.y2 > height + tolerance:
      resolve = True
      y = height - self.size
      if collisionHandling:
        self.speedY = - abs(self.speedY)
    
    if resolve:
      canvas.moveto(self.shape, x, y)
    
    # mit anderem Ball kollidieren, ineffizient checken, keine kombinierte Kollision hier:
    for other in [ball for ball in self.allBalls if ball is not self]:
      collisionDistance, collisionAngle = getDistanceAndAngleBetweenTwoBalls(self, other)
      if collisionDistance <= 0:

        # RPS (Rock Paper Scissors) Logik
        if rpsHandling:
          winner = getRPSWinnerBall(self, other)
          if winner is not None:
            winnerRps = winner.rps
            self.updateRps(winnerRps)
            other.updateRps(winnerRps)

        # Richtungsänderung
        if collisionHandling:
          # Kollision soll zwei Bälle nur beeinflussen, wenn sich einer davon auf den anderen zubewegt
          currentDirectionAngle = angleOfVectorInRadians(self.speedX, -self.speedY)
          otherDirectionAngle = angleOfVectorInRadians(other.speedX, -other.speedY)
          
          if math.cos(currentDirectionAngle - collisionAngle) > 0:
            # Geschwindigkeit spiegeln an Kollisionstangente, außer ein Ball bewegt sich schon von der Kollision weg:
            diffAngles = collisionAngle - currentDirectionAngle
            newDirectionAngle = collisionAngle + math.pi + diffAngles
            self.speedX = self.speed * math.cos(newDirectionAngle)
            self.speedY = - self.speed * math.sin(newDirectionAngle)

          # auch für den anderen Ball:
          reversedCollisionAngle = collisionAngle + math.pi
          if math.cos(otherDirectionAngle - reversedCollisionAngle) > 0:
            otherDiffAngles = reversedCollisionAngle - otherDirectionAngle
            otherNewDirectionAngle = reversedCollisionAngle + math.pi + otherDiffAngles
            other.speedX = other.speed * math.cos(otherNewDirectionAngle)
            other.speedY = - other.speed * math.sin(otherNewDirectionAngle)
  
  def move(self):
    if mode == Mode.RPS_WOBBLE:
      same, prey, avoid = getRPSBallsAll(self, self.allBalls)

      closestPrey = None
      closestPreyDistance = width + height # some huge value
      for other in prey:
        newPreyDistance = getDistanceBetweenTwoBalls(self, other)
        if newPreyDistance < closestPreyDistance:
          closestPreyDistance = newPreyDistance
          closestPrey = other
      chaseDirection = None if closestPrey is None else getAngleBetweenTwoBalls(self, closestPrey)
      closestPreyDistance = 0 if closestPrey is None else closestPreyDistance

      closestAvoid = None
      closestAvoidDistance = width + height # some huge value
      for other in avoid:
        newAvoidDistance = getDistanceBetweenTwoBalls(self, other)
        if newAvoidDistance < closestAvoidDistance:
          closestAvoidDistance = newAvoidDistance
          closestAvoid = other
      fleeDirection = None if closestAvoid is None else getAngleBetweenTwoBalls(closestAvoid, self)
      closestAvoidDistance = 0 if closestAvoid is None else closestAvoidDistance

      chaseAmplifier = 2* math.exp((closestAvoidDistance - closestPreyDistance) / (width / 4)) + 1
      newSpeedX = (0 if fleeDirection is None else math.cos(fleeDirection)) + (0 if chaseDirection is None else (chaseAmplifier * math.cos(chaseDirection)))
      newSpeedY = -(0 if fleeDirection is None else math.sin(fleeDirection)) - (0 if chaseDirection is None else (chaseAmplifier * math.sin(chaseDirection)))

      print(f"{newSpeedX}, {newSpeedY}, {chaseDirection}")

      speedNorm = math.sqrt(newSpeedX**2 + newSpeedY**2)

      normedSpeedX = 0 if speedNorm == 0 else (newSpeedX / speedNorm)
      normedSpeedY = 0 if speedNorm == 0 else (newSpeedY / speedNorm)

      self.speedX = self.speed * (normedSpeedX + (noiseAmplifier * (random.random() - 0.5))) # add some random noise
      self.speedY = self.speed * (normedSpeedY + (noiseAmplifier * (random.random() - 0.5))) # add some random noise

    canvas.move(self.shape, self.speedX, self.speedY)

  def movement(self):
    self.collisionHandling()
    self.move()
    tk.after(40, self.movement) # nach 20 milliseconds wird movement wieder aufgerufen, d.h. wir haben eine Bewegung mit ca (1000/20) = 50 Bildern pro Sekunde (zumindest berechnet, kommt dann noch auf den Bildschirm an)


# Hilfskram für zufällige Bälle
colors = ["red", "green", "blue", "cyan", "magenta", "orange", "yellow", "white", "black", "purple", "yellow"]
def getRandomFromArray(array):
  return array[random.randint(0, len(array)-1)]

# Bälle erstellen
balls = []
for i in range(amount):
  size = random.randint(5, math.floor(min(width, height) / 5)) if mode in [Mode.COLLISIONS] else standardSize
  startX = random.randint(0, width - size)
  startY = random.randint(0, height - size)
  # color = getRandomFromArray(colors)
  # color = colors[i % len(colors)]
  rps = getRandomFromArray(rpsArray)
  speed = random.randint(1, 15) if mode in [Mode.COLLISIONS] else standardSpeed
  direction = degToRadians(random.randint(0, 360))
  balls.append(Ball(startX, startY, size, None, speed, direction, rps, balls))


# tkinter Schleife starten
tk.mainloop()