from tkinter import *
import math

# mit viel Inspiration von https://www.youtube.com/watch?v=IQWfaksVJC4

# tkinter canvas erstellen
width, height = 500, 350
tk = Tk()
canvas = Canvas(tk, width=width, height=height)
canvas.pack()


def getDistanceAndAngleBetweenTwoBalls(ball1, ball2):
  # für die folgenden Berechnungen nehmen wir an, dass y nach oben wächst, nicht nach unten. Genau wie bei allen anderen Stellen, an denen es um Winkel geht, also wie in einem normalen Koordinatensystem mit mathematisch positivem Drehsinn (gegen Uhrzeigersinn)
  distCentersX = ball2.center[0] - ball1.center[0]
  distCentersY = ball1.center[1] - ball2.center[1]
  distCenters = math.sqrt(distCentersX**2 + distCentersY**2)
  distBalls = distCenters - (ball1.radius + ball2.radius)

  if distCenters == 0:
    return (0, 0)

  angleFromBallOneToBallTwo = angleOfVectorInRadians(distCentersX, distCentersY)
  
  return (distBalls, angleFromBallOneToBallTwo)

def angleOfVectorInRadians(x, y):
  arctan = math.atan(y / x)
  angle = arctan + (math.pi if x < 0 else 0)
  return angle


def degToRadians(degrees):
  return (degrees / 360) * 2 * math.pi
def radToDegrees(radians):
  return (radians / (2 * math.pi)) * 360


# Ball class für mehrfache Nutzung erstellen
class Ball:
  def __init__(self, startX, startY, size, color, speed, direction, allBalls):
    # init Parameter speichern für spätere Nutzung außerhalb von __init__
    self.size = size
    self.color = color
    self.speed = speed
    self.direction = direction # im Bogenmaß, nicht in Grad!
    self.allBalls = allBalls

    self.radius = size / 2
    
    # Objekt auf dem Canvas erstellen
    self.oval = canvas.create_oval(startX + 0, startY + 0, startX + size, startY + size, fill=color)
    
    # Geschwindigkeit aus speed und direction berechnen, canvas.move() braucht die Geschwindigkeit in x- und y-Richtung
    self.speedX = speed * math.cos(direction)
    self.speedY = speed * math.sin(direction)

    # direkt nach Erstellung des Balls rufen wir movement auf, um die Bewegung zu starten
    self.movement()
  

  # in einem Schwung die aktuelle Position und die variablen x1, y1, x2, y2 setzen, für leichter lesbaren Code
  def updatePos(self):
    self.pos = canvas.coords(self.oval) # wie schon die Dimensionen des Canvas, sind auch die Dimensionen aller anderen Objekte angegeben durch die obere linke Ecke und die untere rechte Ecke, davon jeweils die x- und y-Koordinate. Enthalten in canvas.coords(...) sind vier Zahlen: 0) x-Koordinate Ecke oben links 1) y-Koordinate Ecke oben links, 2) x-Koordinate Ecke unten rechts, 3) y-Koordinate Ecke unten rechts.
    self.x1 = self.pos[0]
    self.y1 = self.pos[1]
    self.x2 = self.pos[2]
    self.y2 = self.pos[3]
    self.center = [self.x1 + self.radius, self.y1 + self.radius]


  def collisionHandling(self):
    self.updatePos()

    # mit Wand kollidieren:
    if self.x1 < 0:
      self.speedX = abs(self.speedX)
      canvas.moveto(self.oval, 1, self.y1)
    if self.x2 > width:
      self.speedX = - abs(self.speedX)
      canvas.moveto(self.oval, width - self.size - 1, self.y1)
    if self.y1 < 0:
      self.speedY = abs(self.speedY)
      canvas.moveto(self.oval, self.x1, 1)
    if self.y2 > height:
      self.speedY = - abs(self.speedY)
      canvas.moveto(self.oval, self.x1, height - self.size - 1)
    
    # mit anderem Ball kollidieren, ineffizient checken, keine kombinierte Kollision hier:
    for other in [ball for ball in self.allBalls if ball is not self]:
      collisionDistance, collisionAngle = getDistanceAndAngleBetweenTwoBalls(self, other)
      if collisionDistance <= 0:

        currentDirectionAngle = angleOfVectorInRadians(self.speedX, -self.speedY)
        otherDirectionAngle = angleOfVectorInRadians(other.speedX, -other.speedY)

        # Kollision soll zwei Bälle nur beeinflussen, wenn sich einer davon auf den anderen zubewegt
        if math.cos(currentDirectionAngle - collisionAngle) > 0 or math.cos(otherDirectionAngle - (-collisionAngle)) > 0:

          print(f"{self.color} collided with {other.color}, collisionAngle: {radToDegrees(collisionAngle)}°, angleDiff: {currentDirectionAngle - collisionAngle}, cos: {math.cos(currentDirectionAngle - collisionAngle)}")

          if math.cos(currentDirectionAngle - collisionAngle) > 0:
            # # Kollision auflösen, dafür entsprechend der eigenen Geschwindigkeit auseinander schieben:
            # factorSelf = self.speed / (self.speed + other.speed)
            # canvas.moveto(
            #   self.oval,
            #   self.x1 - (abs(collisionDistance) * math.cos(collisionAngle) * factorSelf),
            #   self.y1 + (abs(collisionDistance) * math.sin(collisionAngle) * factorSelf)
            # )

            # Geschwindigkeit nach Kollision spiegeln an Kollisionstangente, außer ein Ball bewegt sich schon von der Kollision weg:
            diffAngles = collisionAngle - currentDirectionAngle
            newDirectionAngle = collisionAngle + math.pi + diffAngles
            self.speedX = self.speed * math.cos(newDirectionAngle)
            self.speedY = - self.speed * math.sin(newDirectionAngle)

          # auch für den anderen Ball:
          reversedCollisionAngle = collisionAngle + math.pi
          if math.cos(otherDirectionAngle - reversedCollisionAngle) > 0:
            # factorOther = other.speed / (self.speed + other.speed)
            # canvas.moveto(
            #   other.oval,
            #   other.x1 + (abs(collisionDistance) * math.cos(collisionAngle) * factorOther),
            #   other.y1 - (abs(collisionDistance) * math.sin(collisionAngle) * factorOther)
            # )

            otherDiffAngles = reversedCollisionAngle - otherDirectionAngle
            otherNewDirectionAngle = reversedCollisionAngle + math.pi + otherDiffAngles
            other.speedX = other.speed * math.cos(otherNewDirectionAngle)
            other.speedY = - other.speed * math.sin(otherNewDirectionAngle)



  def movement(self):
    self.collisionHandling()
    canvas.move(self.oval, self.speedX, self.speedY)
    tk.after(20, self.movement) # nach 20 milliseconds wird movement wieder aufgerufen, d.h. wir haben eine Bewegung mit ca 50 Bildern pro Sekunde (zumindest berechnet, kommt dann noch auf den Bildschirm an)
  
  

# Bälle erstellen
balls = []
balls.append(Ball(0, 0, 70, "red", 12, degToRadians(70), balls))
balls.append(Ball(4 * width / 4, 2 * height / 4, 20, "orange", 3, degToRadians(10), balls))
balls.append(Ball(3 * width / 4, 3 * height / 4, 50, "green", 6, degToRadians(-50), balls))
balls.append(Ball(2 * width / 4, 4 * height / 4, 100, "black", 2.5, degToRadians(100), balls))
balls.append(Ball(1 * width / 4, 3 * height / 4, 80, "white", 15, degToRadians(150), balls))
balls.append(Ball(2 * width / 4, 2 * height / 4, 35, "cyan", 2, degToRadians(130), balls))

# tkinter Schleife starten
tk.mainloop()