# Menger-Schwamm

Informationen zum Menger-Schwamm gibt es [Hier](https://de.wikipedia.org/wiki/Menger-Schwamm).

![Menger-Schwamm 0. Iteration (Quadrat)](../images/menger-png/menger-0-farbig-ausgefuellt.png "Menger It. 0") ![Menger-Schwamm 1. Iteration](../images/menger-png/menger-1-farbig-ausgefuellt.png "Menger It. 1") ![Menger-Schwamm 2. Iteration](../images/menger-png/menger-2-farbig-ausgefuellt.png "Menger It. 2") ![Menger-Schwamm 3. Iteration](../images/menger-png/menger-3-farbig-ausgefuellt.png "Menger It. 3") ![Menger-Schwamm 3. Iteration](../images/menger-png/menger-4-thin.png "Menger It. 4")   

Ganz wichtig: Immer, wenn wir ein neues Manöver in `shapes.py` programmiert haben, wollen wir dieses ausprobieren. Das hilft uns zu sehen, ob wir Fehler gemacht haben und ob das gewünschte Ergebnis herauskommt. Ab hier wird nicht mehr extra daran erinnert. Trotzdem werden wir jedes neu definierte Manöver in unserer `main.py` testen!

Zuerst einmal malen wir nur ein Quadrat. Also Stift ansetzen, 4 mal geradeaus gehen und dabei nach jedem Mal im rechten Winkel abbiegen (hier nach rechts), und den Stift wieder hochheben:
```python
def menger(turtle, laenge):
  turtle.pendown()
  
  for i in range(4):
    turtle.forward(laenge)
    turtle.right(90)
  
  turtle.penup()
```

Als nächstes wollen wir, genau wie beim Code für das Menger-Schwamm, statt einem großen Quadrat, mehrere kleine Quadrate aneinanderreihen. Das auch im richtigen Muster und schon haben wir den ersten Iterationsschritt vom Menger-Schwamm. Dafür lagern wir zunächst die Anweisung des Quadrat Malens in ein eigenes Manöver `quadrat` aus, da wir viele Quadrate malen werden:
```python
def quadrat(turtle, laenge):
  for i in range(4):
    turtle.forward(laenge)
    turtle.right(90)
```

Und dann ordnen wir die Quadrate passend an in `menger`. Das sieht nach viel Code aus, aber kompliziert ist er nicht. Wir müssen nur sicherstellen, dass sich unsere Schildkröte an die richtige Ecke des gerade gemalten Quadrates bewegt, um dort das nächste Quadrat loszumalen. Die Seitenlänge der kleineren Quadrate ist genau ein Drittel so lang, wie die Seitenlänge des größeren Quadrats. **Tipp:** Im folgenden Code bewegt sich die Schildkröte "außen" um das große Quadrat herum. Für leicht geänderte Menger-Schwämme, wie den Parallelogramm-Schwamm (siehe unten), ist es praktisch, wenn deine Schildkröte immer an der oberen linken Ecke des Quadrates steht, das sie gerade malt:
```python
def menger(turtle, laenge):
  turtle.pendown()
  
  quadrat(turtle, laenge/3)
  turtle.forward(laenge/3)
  
  quadrat(turtle, laenge/3)
  turtle.forward(laenge/3)
  
  quadrat(turtle, laenge/3)
  turtle.forward(laenge/3)
  turtle.right(90)
  turtle.forward(laenge/3)
  
  quadrat(turtle, laenge/3)
  turtle.forward(laenge/3)
  
  quadrat(turtle, laenge/3)
  turtle.forward(laenge/3)
  turtle.right(90)
  turtle.forward(laenge/3)
  
  quadrat(turtle, laenge/3)
  turtle.forward(laenge/3)
  
  quadrat(turtle, laenge/3)
  turtle.forward(laenge/3)
  turtle.right(90)
  turtle.forward(laenge/3)
  
  quadrat(turtle, laenge/3)
  turtle.forward(laenge/3)
  turtle.forward(laenge/3)
  turtle.right(90)
  
  turtle.penup()
```

Als nächstes bringen wir Rekursion ins Spiel. Das heißt wir rufen einfach statt `quadrat` wieder `menger` in sich selbst auf, um rekursiv immer kleinere Quadrate zu malen (**Achtung**, funktioniert so noch nicht ganz! Siehe dafür den nächsten Text und Code):
```python
def menger(turtle, laenge):
  turtle.pendown()
  
  menger(turtle, laenge/3)
  turtle.forward(laenge/3)
  
  menger(turtle, laenge/3)
  turtle.forward(laenge/3)
  
  menger(turtle, laenge/3)
  turtle.forward(laenge/3)
  turtle.right(90)
  turtle.forward(laenge/3)
  
  menger(turtle, laenge/3)
  turtle.forward(laenge/3)
  
  menger(turtle, laenge/3)
  turtle.forward(laenge/3)
  turtle.right(90)
  turtle.forward(laenge/3)
  
  menger(turtle, laenge/3)
  turtle.forward(laenge/3)
  
  menger(turtle, laenge/3)
  turtle.forward(laenge/3)
  turtle.right(90)
  turtle.forward(laenge/3)
  
  menger(turtle, laenge/3)
  turtle.forward(laenge/3)
  turtle.forward(laenge/3)
  turtle.right(90)
  
  turtle.penup()
```

Damit wir das aber nicht bis in alle Ewigkeit machen (das ist sowieso nicht möglich!), bestimmen wir ein unteres Limit `limitmin`. Ist dieses erreicht, läuft unsere Schildkröte einfach wieder ein Quadrat ab, anstatt nochmal `menger` aufzurufen:
```python
def menger(turtle, laenge, limitmin):
  turtle.pendown()
  
  if laenge < limitmin*3:
    #turtle.begin_fill()
    quadrat(turtle, laenge)
    #turtle.end_fill()

  else:
    menger(turtle, laenge/3, limitmin)
    turtle.forward(laenge/3)
  
    menger(turtle, laenge/3, limitmin)
    turtle.forward(laenge/3)
  
    menger(turtle, laenge/3, limitmin)
    turtle.forward(laenge/3)
    turtle.right(90)
    turtle.forward(laenge/3)
  
    menger(turtle, laenge/3, limitmin)
    turtle.forward(laenge/3)
  
    menger(turtle, laenge/3, limitmin)
    turtle.forward(laenge/3)
    turtle.right(90)
    turtle.forward(laenge/3)
  
    menger(turtle, laenge/3, limitmin)
    turtle.forward(laenge/3)
  
    menger(turtle, laenge/3, limitmin)
    turtle.forward(laenge/3)
    turtle.right(90)
    turtle.forward(laenge/3)
  
    menger(turtle, laenge/3, limitmin)
    turtle.forward(laenge/3)
    turtle.forward(laenge/3)
    turtle.right(90)
  
  turtle.penup()
```

Um jetzt noch die kleinsten Quadrate auszufüllen, wie wir es normalerweise vom Menger-Schwamm kennen, müssen wir nur im Code hierüber bei den Kommentaren die `#` entfernen, also direkt vor und nach `quadrat(turtle, laenge)`.


## Unser Beispielcode sieht am Ende so aus: 

`main.py`:
```python
from turtle import *
from shapes import *

inge = Turtle()
inge.shape("turtle")
inge.speed(150)

inge.penup()
inge.goto(-100, 130)

menger(inge, 200, 20)

inge.goto(0, -100)
inge.write("Mein Menger-Schwamm :-)", None, "center", "15pt bold")
inge.goto(0, -115)
inge.seth(0)
```


`shapes.py`:
```python
import turtle

def menger(turtle, laenge, limitmin):
  turtle.pendown()
  
  if laenge < limitmin*3:
    quadrat(turtle, laenge)

  else:
    menger(turtle, laenge/3, limitmin)
    turtle.forward(laenge/3)
  
    menger(turtle, laenge/3, limitmin)
    turtle.forward(laenge/3)
  
    menger(turtle, laenge/3, limitmin)
    turtle.forward(laenge/3)
    turtle.right(90)
    turtle.forward(laenge/3)
  
    menger(turtle, laenge/3, limitmin)
    turtle.forward(laenge/3)
  
    menger(turtle, laenge/3, limitmin)
    turtle.forward(laenge/3)
    turtle.right(90)
    turtle.forward(laenge/3)
  
    menger(turtle, laenge/3, limitmin)
    turtle.forward(laenge/3)
  
    menger(turtle, laenge/3, limitmin)
    turtle.forward(laenge/3)
    turtle.right(90)
    turtle.forward(laenge/3)
  
    menger(turtle, laenge/3, limitmin)
    turtle.forward(laenge/3)
    turtle.forward(laenge/3)
    turtle.right(90)
  
  turtle.penup()


def quadrat(turtle, laenge):
  for i in range(4):
    turtle.forward(laenge)
    turtle.right(90)
```


## So könnte `menger` aussehen mit Iterationsanzahl `schritte` als Eingabeparameter, statt mindester Strichlänge `limitmin`
```python
def menger(turtle, laenge, schritte):
  turtle.pendown()
  
  if schritte < 1:
    quadrat(turtle, laenge)

  else:
    menger(turtle, laenge/3, schritte-1)
    turtle.forward(laenge/3)
  
    menger(turtle, laenge/3, schritte-1)
    turtle.forward(laenge/3)
  
    menger(turtle, laenge/3, schritte-1)
    turtle.forward(laenge/3)
    turtle.right(90)
    turtle.forward(laenge/3)
  
    menger(turtle, laenge/3, schritte-1)
    turtle.forward(laenge/3)
  
    menger(turtle, laenge/3, schritte-1)
    turtle.forward(laenge/3)
    turtle.right(90)
    turtle.forward(laenge/3)
  
    menger(turtle, laenge/3, schritte-1)
    turtle.forward(laenge/3)
  
    menger(turtle, laenge/3, schritte-1)
    turtle.forward(laenge/3)
    turtle.right(90)
    turtle.forward(laenge/3)
  
    menger(turtle, laenge/3, schritte-1)
    turtle.forward(laenge/3)
    turtle.forward(laenge/3)
    turtle.right(90)
  
  turtle.penup()
```
Wie man sieht, fast genau so wie vorher! Geändert wurde tatsächlich nur genau jede Stelle, an der vorher das Wort `limitmin` stand und die Bedingung bei der *if*-Abfrage. Der Rest des Codes ist wie zuvor.



# Bonusaufgaben

Du hast die Anleitung zum Menger-Schwamm verstanden oder den ganzen Schwamm sogar erfolgreich selbst programmiert? Dann versuche dich an diesen Bonusaufgaben! Ändere deinen Code so, dass du solche oder ähnliche Schwämme erzeugen kannst. Zur Hilfe wird die Idee hinter jedem Bild beschrieben.   

Falls du selbst auf interessante Variationen des Menger-Schwamms kommst, kannst du mir sehr gerne dein Ergebnis unter alex.mai@posteo.net zuschicken. Vielleicht schafft es deine Variation ja in diese Bonusaufgabensammlung! Und falls du meine Lösungsvorschläge zu den Bonusaufgaben sehen magst, zum Beispiel um den Fehler in deinem Programm zu finden, kannst du mich auch unter alex.mai@posteo.net anschreiben.


### Quadrate mit zufälligen Farben ("Regenbogen-Schwamm")
Hier soll einfach jedes kleinste Quadrat, das nicht ein Loch ist, in einer zufälligen Farbe gefüllt und umrahmt werden. Eine Vorlage zur Erzeugung von zufälligen Farben für Inge findest du in der [einführenden Python Turtle Anleitung, ganz unten bei "Lustige Beispiele"](/Malen-in-Python-Turtle/README.md/#lustige-beispiele).   
![Menger-Schwamm mit zufälligen Farben bei jedem Quadrat, 1 It.](../images/menger-png/menger-1-bunt.png "Menger bunt, 1 It.") ![Menger-Schwamm mit zufälligen Farben bei jedem Quadrat, 2 It.](../images/menger-png/menger-2-bunt.png "Menger bunt, 2 It.") ![Menger-Schwamm mit zufälligen Farben bei jedem Quadrat, 3 It.](../images/menger-png/menger-3-bunt.png "Menger bunt, 3 It.")   


### Menger in sich selbst ("Stufen-Schwamm")
Hier sollen in einem Muster, das dir gefällt, hier und da verschieden viele Iterationen des Schwamms im Schwamm auftauchen. Ich habe mich für folgendes Muster entschieden.   
![Menger-Schwamm Stufen](../images/menger-png/menger-stufig.png "Menger stufig")   


### Quadrate mit verschiedenen Winkeln ("Parallelogramm-Schwamm")
Hier soll der Winkel der Quadrate angepasst werden, sodass daraus Rauten werden. Weiterhin kannst du den Code gleich so ändern, dass du auch die Breite und Höhe der Rauten ändern kannst und somit aus deinen Quadraten schon Parallelogramme gemacht hast.   
![Menger-Schwamm Raute, 0. It](../images/menger-png/menger-0-raute.png "Menger Raute, 0. It") ![Menger-Schwamm Raute, 1. It](../images/menger-png/menger-1-raute.png "Menger Raute, 1 It") ![Menger-Schwamm Raute, 2. It](../images/menger-png/menger-2-raute.png "Menger Raute, 2 It") ![Menger-Schwamm Raute, 3. It](../images/menger-png/menger-3-raute.png "Menger Raute, 3 It")   


### "Würfel-Schwamm"
Um das zu schaffen, brauchst du wahrscheinlich den Code für den Parallelogramm-Schwamm. Denn eigentlich muss man nur an den passenden Stellen Parallelogramm-Schwämme aneinanderhängen, jeweils mit den richtigen Winkeln!   
![Menger-Schwamm Würfel grün, 1 It.](../images/menger-png/menger-wuerfel1it.png "Menger-Würfel 1 It") ![Menger-Schwamm Würfel blau, 2 It.](../images/menger-png/menger-wuerfel2it.png "Menger-Würfel 2 It") ![Menger-Schwamm Würfel pink, 3 It.](../images/menger-png/menger-wuerfel3it.png "Menger-Würfel 3 It") ![Menger-Schwamm Würfel gelb, 2 It., mit hinteren Flächen](../images/menger-png/menger-wuerfel2it-krass.png "Menger-Würfel 2 It, mit hinteren Flächen")   
