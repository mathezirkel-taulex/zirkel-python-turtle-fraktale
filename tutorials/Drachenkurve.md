# Drachenkurve

Informationen zur Drachenkurve gibt es [Hier](https://de.wikipedia.org/wiki/Drachenkurve).   

**Achtung!** Dieses Fraktal ist sehr schwierig zu programmieren. Versuche dich vorher auf jeden Fall an einem der anderen Fraktale.

Malt man sich eine Drachenkurve auf, erkennt man folgende Vorschrift zum Abbiegen:

1. Iteration: R
2. Iteration: R R L
3. Iteration: R R L R R L L
4. Iteration: R R L R R L L R R R L L R L L.

Daraus erkennt man, dass wir bei jedem Schritt zuerst ein R hinzufügen und dann alle Schritte der letzten Iteration, aber in umgekehrter Reihenfolge und alle Ls und Rs wurden vertauscht.   

Wir brauchen also einen Code, der unsere letzte Iteration nimmt, ein R hinzufügt, und dann unsere letzte Iteration umkehrt und Ls und Rs vertauscht. Dafür verwenden wir eine Liste.   

Eine Liste in Python speichert uns mehrere Zahlen oder Wörter oder Buchstaben (was wir wollen!) in geordneter Reihenfolge ab. Das heißt, wenn wir eine Liste `beispielListe` erstellen und A, 7, D und C in dieser Reihenfolge reinstecken und nacheinander auf die Elemente zugreifen, dann kriegen wir sie auch in der Reihenfolge A, 7, D, C wieder raus. Das könnten wir so machen:
```python
beispielListe = ["A", 7, "D", C]
```

Hierbei haben wie A und D als Buchstaben gespeichert, da wir sie in Anführungszeichen geschrieben haben. C wird hier als Variable verstanden. Damit diese Liste erstellt werden kann, muss vorher eine Variable C existieren! Wir werden für die Drachenkurve aber nur Buchstaben speichern, also immer in Anführungszeichen "".

Unsere Liste nennen wir `reihenfolge` und befüllen sie zunächst mit einem "R". Das R verarbeiten wir später mit unserem Code als Anweisung, als nächstes nach rechts abzubiegen (genau so dann auch mit L). Die Anführungszeichen "" brauchen wir, damit Python versteht, dass es sich bei R nicht um eine Variable, sondern um eine Abfolge von Buchstaben handelt. In unserem Fall ist es nur ein einziger Buchstabe.   
```python
reihenfolge = ["R"]
```

Als nächstes schreiben wir uns eine Funktion, die eine beliebige Liste voller L und R nimmt, und von dieser Liste die Reihenfolge vertauscht und dann auch noch alle L mit R vertauscht, und alle R mit L. Hier nennen wir sie `LRListeVertauschenUndUmkehren`. Was sie tut, sieht man unten im fertigen Code.   

Dann nehmen wir die schon existierende Liste `reihenfolge` und fügen ein "R" und die umgekehrte und vertauschte Liste hinzu. Das machen wir in der Funktion `abbiegeGenerator`.

Als letztes brauchen wir unser Manöver `drachenkurve`. Der Eingabeparameter `laenge` bestimmt die Länge der einzelnen kleinen Striche. `faltungen` bestimmt, wie oft man ein Papier in der Mitte falten müsste, um diese Drachenkurve herauszukriegen. Im Manöver erzeugen wir uns eine Liste `vorschrift` mit Hilfe unserer Funktion `abbiegeGenerator`. Und dann geht es auch schon los: zuerst ein Stück vorwärts und dann immer die Liste `vorschrift` nach jedem Schritt fragen, wohin es weiter gehen soll. Der Rest erklärt sich aus den Kommentaren im unteren Code.


## Unser Beispielcode sieht am Ende so aus: 

`main.py`:
```python
from turtle import *
from shapes import *

inge = Turtle()
inge.shape("turtle")
inge.speed(100)

inge.color("darkgreen")

inge.penup()
inge.goto(-80, 0)
inge.seth(0)

drachenkurve(inge, 5, 10)

inge.goto(-40, -90)
inge.write("Meine Drachenkurve :-)", None, "center", "15pt bold")
inge.goto(-40, -110)
inge.seth(0)
```


`shapes.py` (großzügig kommentiert):
```python
import turtle
import copy

def drachenkurve(turtle, laenge, faltungen):
  turtle.pendown()
  
  # in diesem "Liste" genannten Objekt befinden sich ganz viele "L"s und "R"s, 
  #die meiner Schildkröte vorgeben, wie sie abbiegen muss. abbiegeGenerator 
  #habe ich unten selbst geschrieben
  vorschrift = abbiegeGenerator(faltungen)
  
  turtle.forward(laenge)
  
  # hier wird in der while-Schleife geprüft, ob die Liste noch Inhalt hat. 
  #Wenn sie leer ist, wird False zurückgegeben und die Schleife wird beendet.
  while vorschrift:
    # pop() nimmt den nächsten Eintrag aus der Liste und entfernt ihn
    #gleichzeitig aus der Liste, sodass beim nächsten pop() auch der nächste
    #Eintrag genommen wird, bis die Liste leer ist
    naechsteDrehung = vorschrift.pop()
    
    # Wir brauchen hier zwei =-Symbole, da wir hier fragen, ob Gleichheit
    #besteht. Mit nur einem Gleichheitsymbol würden wir sagen, dass 
    #naechsteDrehung gleich "L" ist, also eine Zuweisung bzw. eine Definition
    #vornehmen.
    if naechsteDrehung == "L":
      turtle.left(90)
    else:
      turtle.right(90)
    turtle.forward(laenge)
  
  turtle.penup()


def abbiegeGenerator(faltungen):
  # Hier erstelle ich eine Liste mit dem Namen "reihenfolge", in welcher 
  #beschrieben steht, in welcher Reihenfolge man nach rechts (R) und links (L) 
  #abbiegen muss
  reihenfolge = ["R"]
  
  # Jetzt befülle ich meine Liste nach der Drachenkurvenvorschrift: Bei jeder 
  #Iteration nehmen wir die letzte Iteration, fügen ein R hinzu, und dann noch 
  #die letzte Iteration aber umgekehrt und Ls und Rs vertauscht.
  for i in range(faltungen - 1):
    reihenfolgeUmgekehrtUndVertauscht = LRListeVertauschenUndUmkehren(reihenfolge)
    reihenfolge.append("R")
    reihenfolge = reihenfolge + reihenfolgeUmgekehrtUndVertauscht
  
  return reihenfolge
    
    
def LRListeVertauschenUndUmkehren(liste):
  neueListe = []
  
  # Reihenfolge der Einträge von liste umkehren:
  tempListe = copy.deepcopy(liste)
  tempListe.reverse()
  
  # und hier die umgekehrte Liste durchgehen und an unsere neueListe ein L 
  #anhängen, wenn in der liste ein R steht, und ein R anhängen, wenn in der 
  #liste ein L steht
  for i in range(len(tempListe)):
    if tempListe[i] == "L":
      naechsterEintrag = "R"
    else:
      naechsterEintrag = "L"
    neueListe.append(naechsterEintrag)
  
  return neueListe
```


Und hier nochmal `shapes.py` komplett ohne Kommentare, aber sonst genau wie hierüber:
```python
import turtle
import copy

def drachenkurve(turtle, laenge, faltungen):
  turtle.pendown()
  
  vorschrift = abbiegeGenerator(faltungen)
  turtle.forward(laenge)
  
  while vorschrift:
    naechsteDrehung = vorschrift.pop()
    
    if naechsteDrehung == "L":
      turtle.left(90)
    else:
      turtle.right(90)
    turtle.forward(laenge)
  
  turtle.penup()


def abbiegeGenerator(faltungen):
  reihenfolge = ["R"]
  
  for i in range(faltungen - 1):
    reihenfolgeUmgekehrtUndVertauscht = LRListeVertauschenUndUmkehren(reihenfolge)
    reihenfolge.append("R")
    reihenfolge = reihenfolge + reihenfolgeUmgekehrtUndVertauscht
  
  return reihenfolge
    
    
def LRListeVertauschenUndUmkehren(liste):
  neueListe = []
  tempListe = copy.deepcopy(liste)
  tempListe.reverse()
  
  for i in range(len(tempListe)):
    if tempListe[i] == "L":
      naechsterEintrag = "R"
    else:
      naechsterEintrag = "L"
    neueListe.append(naechsterEintrag)
  
  return neueListe
  ```
    
