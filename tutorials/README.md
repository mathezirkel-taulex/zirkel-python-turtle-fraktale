# Fraktale Malen in Python mit [trinket.io](https://trinket.io)

Der Einfachheit halber machen wir im Folgenden alles in der Programmierumgebung von [trinket.io](https://trinket.io), direkt benutzbar, ohne vorher irgendetwas installieren zu müssen. Entsprechend wird für alle Codeschnipsel hier angenommen, dass die Schildkröte auf einer Malfläche – auch *Canvas* genannt – malt, die ca. 200 Pixel breit und hoch ist. Die meisten Sachen werden sich nicht weiter als 150px rausbewegen.   

Du hast Verbesserungsvorschläge für diese oder andere Anleitungen hier? Dann schreib mir gerne eine E-Mail, unter alex.mai@posteo.net erreichst du mich :-) Schreib in den Betreff "Python Turtle", damit ich deine E-Mail besser zuordnen kann.


## Anleitungen zu Fraktalen

Wenn du schon Erfahrung hast mit der Programmierung der Schildkröte, kannst du dich direkt an den Fraktalen versuchen. Ansonsten empfehle ich dir, dich zuerst mithilfe der untenstehenden Anleitung mit der Schildkröte anzufreunden. Wie man die Fraktale Schritt für Schritt programmiert (vorher gerne alleine versuchen, vielleicht schaffst du es ja!), siehst du, wenn du auf den entsprechenden Fraktalnamen klickst. Am Ende der einzelnen Anelitungen gibt es auch fordernde Bonusaufgaben:
+ [Kochsche Schneeflocke](./Kochsche-Schneeflocke.md)
+ [Sierpinski-Dreieck](./Sierpinski-Dreieck.md)
+ [Menger-Schwamm](./Menger-Schwamm.md)
+ [Drachenkurve](./Drachenkurve.md) (Sehr schwer!)


## Inhaltsverzeichnis

[[_TOC_]]



Schildkröten in der Programmiersprache Python sehen nicht nur süß aus, sondern können auch schon fast alles, was wir brauchen, um Fraktale und andere tolle Dinge zu malen. Dafür machen wir uns zuerst mit der Seite [trinket.io](https://trinket.io) ein wenig vertraut und schreiben dann auch schon unseren ersten Code.


# Kurze Einführung in den Beispielcode auf [trinket.io](https://trinket.io)

Wenn du [trinket.io](https://trinket.io) geöffnet hast und ein bisschen runterscrollst, solltest du ein Fenster mit Code auf der linken Seite und einer Schildkröte mit bunten Kreisen auf der rechten Seite sehen.

![Programmierfläche auf trinket.io](../images/other/trinket-default.png "Programmierfläche auf trinket.io")

Links ist der Beispielcode, der immer da ist, wenn du [trinket.io](https://trinket.io) besuchst. Über dem Code ist eine Leiste mit verschiedenen Knöpfen. Wir brauchen den "Play"-Knopf – mit dem nach rechts zeigenden Dreieck – sehr oft, also probiere ihn gerne mal aus. Wenn du ihn drückst, wird der Code von links auf der Malfläche rechts ausgeführt.    

Wenn du gerne erst ausprobierst, und dann nachliest, dann schau dir das erst einmal an und Spiele mit dem Code herum.    

Es ist aber nicht nur der Code den du in der Textdatei `main.py` siehst, sondern noch weiterer Code in `shapes.py`. Das schauen wir uns später genauer an, nachdem wir uns unsere Schildkröte angesehen haben, [weiter unten](#verschiedene-dateien).    


Wir schauen uns nur ganz grob an, was der Beispielcode enthält. Die ersten paar Zeilen können wir nämlich auch gut gebrauchen. Beachte dabei, dass wir an dieser Stelle auf eine vertiefte Erklärung verzichten.

Zuerst fällt dir vielleicht auf, dass verschiedene Farben im Code auftauchen. Diese Farben werden nur für dich angezeigt. Also für den Menschen, der am Code arbeitet. Für deinen Computer oder das Programm gibt es diese Farben nicht. Sie werden nur für deinen Lesekomfort angezeigt und vermitteln praktische Informationen.

Startest du zum Beispiel eine Zeile mit dem Hash- beziehungsweise dem Rauten-Symbol `#`, dann wird diese ganze Zeile vom Programm ignoriert und für dich in grüner Farbe angezeigt. In der Fachsprache nennen wir so eine Zeile einen *"Kommentar"*, weil du in so einer Zeile Kommentare und Anmerkungen für andere Menschen oder dein zukünftiges Ich hinterlassen kannst. Und das ohne, dass diese Zeile das Programm beeinflusst. 

Hier nochmal eine Kopie vom Beispielcode:
```python
# You can edit this code and run it right here in the browser!
# First we'll import some turtles and shapes: 
from turtle import *
from shapes import *

# Create a turtle named Tommy:
tommy = Turtle()
tommy.shape("turtle")
tommy.speed(7)

# Draw three circles:
draw_circle(tommy, "green", 50, 25, 0)
draw_circle(tommy, "blue", 50, 0, 0)
draw_circle(tommy, "yellow", 50, -25, 0)

# Write a little message:
tommy.penup()
tommy.goto(0,-50)
tommy.color("black")
tommy.write("Teach With Code!", None, "center", "16pt bold")
tommy.goto(0,-80)

# Try changing draw_circle to draw_square, draw_triangle, or draw_star
```

Die erste nicht-kommentierte Zeile, `from turtle import *`, erlaubt uns den Einsatz von Schildkröten. Es ist nämlich so, dass die eingesetzte Programmiersprache *Python* nicht nur für Schildkröten geeignet ist, sondern auch für viele andere Dinge, wie Internetwebseiten, Anwendungen für Mobilgeräte, wissenschaftliche Berechnungen und Simulationen, oder sogar einfache Videospiele. Damit nicht immer alle Funktionalitäten, die es gibt, gleichzeitig geladen sind und beim Programmieren für Chaos sorgen, tut man sich gewünschte Funktionalitäten durch so eine `import`-Zeile in den Code hereinladen. Alle Grundfunktionalitäten von Python sind natürlich immer automatisch geladen.

Die nächste `import`-Zeile importiert den ganzen Code, der in der Datei `shapes.py` steht. Mehr dazu später [weiter unten](#verschiedene-dateien).

Danach kommen drei weitere für uns extrem wichtige Zeilen. In der Zeile `tommy = Turtle()` steht *"Ab jetzt existiert eine Schildkröte, und 'tommy' ist ihr Name."*. Beim Programmieren bedeutet das `=`-Symbol eine Zuweisung. Links vom `=` steht normalerweise immer ein von dir selbstgewählter Name, eine sogenannte *Variable*. und rechts davon steht, was hinter dieser Variable steckt. In diesem Fall steckt hinter der Variable mit dem Namen `tommy`, eine 'Turtle', also zu Deutsch: eine Schildkröte. Die runden Klammern dahinter nehmen wir erst einmal so hin.

Die Zeile `tommy.shape("turtle")` bestimmt das Aussehen der Schildkröte. Folgende Werte kannst du verwenden: `"classic"`, `"arrow"`, `"circle"`, `"square"`, `"triangle"`, `"turtle"`

![Ein kurzes Video zeigt eine Schildkröte, die ein Hexagon malt und dabei für jede Seite eine andere Form annimmt.](https://media.geeksforgeeks.org/wp-content/uploads/20200714104344/shape.gif "Demonstration verschiedener Formen der Schildkröte")

Die Zeile `tommy.speed(7)` bestimmt die Geschwindigkeit der Schildkröte.

Die nächsten drei Zeilen, die alle mit `draw_circle` beginnen, rufen jeweils ein Manöver auf, das in der Datei `shapes.py` definiert ist. Dem Manöver werden in den runden Klammern verschiedene Eingabewerte gegeben, wie der Name der ausführenden Schildkröte, die Farbe des Kreises, die Größe des Kreises, und die Position des Kreises. Durch diese verschiedenen Eingabewerte, kann mit dreimal dem gleichen Manöver jeweils etwas anderes gemalt werden. Später lernen wir, wie wir eigene Manöver definieren mit verschiedenen Eingabewerten.


## Benennung der Schildkröte und sonstiger Variablen

Den Namen der Schildkröte kannst du übrigens auch beliebig anders wählen. Für die Benennung der Schildkröte und auch für die Benennung jeder anderer Schildkröte musst du folgendes beachten:

* Leerzeichen sind nicht möglich
* vermeide Umlaute, scharfes ß und sonstige Buchstaben, die es in der englischen Sprache nicht gibt
* vermeide Sonderzeichen, die bereits eine Funktion in der Programmiersprache haben, wie Rechnensymbole: `+`, `-`, `*`, `/` und einige andere
* an sich sind Groß- und Kleinschreibung beliebig einsetzbar
* aus Tradition beginnen Variablennamen meistens mit einem Kleinbuchstaben und der Rest ist auch klein

Wenn du deine Schildkröte anders nennen willst, kannst du jedes `tommy` durch den neuen Namen ersetzen.

Wenn du deine Schildkröte zum Beispiel *"Tolle fetzige Schildkröte"* nennen möchtest, gibt es verschiedene Möglichkeiten, den Abstand trotzdem sichtbar zu machen. Bei Python ist es Tradition, dafür ein `_` zu verwenden, also ein *Unterstrich*. Die Variable hieße dann `tolle_fetzige_schildkroete`. Bei anderen Programmiersprachen gibt es andere Traditionen, wie den ersten Buchstaben von jedem weiteren Wort groß zu schreiben. Die Variable hieße dann `tolleFetzigeSchildkroete`. Verwende für dich einfach das, was dir am liebsten gefällt.


# Turtle Befehle

Alles, was Schildkröten bereits von Geburt an können, machen sie, indem wir zuerst den von uns gegebenen Namen der Schildkröte schreiben – In dieser Anleitung heißt die Schildkröte **inge** – gefolgt von einem Punkt, dann die Bezeichnung des Befehls, und dann in runden Klammern einen oder mehrere passende Werte, die zum Befehl passen.


## Grundlegende Befehle

Im Folgenden die wichtigsten Befehle, die wir einer Schildkröte geben können. Bei den meisten Befehlen handelt es sich um einfache englische Wörter. Für dein Verständnis steht eine deutsche Übersetzung jeweils unter dem Code:

+ `inge.forward(42)`   
*"vorwärts"*  
Inge geht 42 Pixel vorwärts, entlang der aktuellen Blickrichtung.

+ `inge.right(120)`   
*"rechts"*  
Inge dreht sich um 120 Grad nach rechts.

+ `inge.left(90)`   
*"links"*  
Inge dreht sich um 90 Grad nach links.

Das waren bereits alle Befehle, die du brauchst, um Fraktale zu malen. Diese drei Befehle solltest du dir unbedingt merken. Alle weiteren Befehle, die hierunter noch stehen, kannst du dir gerne mal ansehen, wirst du aber selten oder gar nicht brauchen.

### Übungsaufgabe
Male mit der Schildkröte ein Quadrat. Wenn du das geschafft hast, probiere ein gleichseitiges Dreieck, und danach ein gleichmäßiges Sechseck.

Die Lösung zu dieser Übungsaufgabe findest du im ersten Beispielcode, im Kapitel [Übungsaufgaben und Beispiele](#übungsaufgaben-und-beispiele).


## Weitere interessante Befehle

+ `inge.speed(77)`   
*"Geschwindigkeit"*  
Ändert die Geschwindigkeit auf `77`. Je höher die Zahl, umso schneller. Ausnahme: Geschwindigkeit `0` ist super schnell.

+ `inge.penup()`   
*"Stift hoch"*  
Hiermit hebt Inge ihren Stift und hinterlässt so beim Umherlaufen keine Spur.

+ `inge.pendown()`   
*"Stift runter"*  
Hiermit setzt Inge ihren Stift wieder ab und hinterlässt beim Umherlaufen eine Spur. Wenn du willst, dass Inge etwas für dich malt, musst du sie nur mit abgesetztem Stift umherlaufen lassen.

+ `inge.goto(-50, 31.415)`   
*"gehe zu"*  
Inge bewegt sich an die Stelle `(-50, 31.415)`, wobei -`50` hier die x-Koordinate ist und `31.415` die y-Koordinate. Kommazahlen werden hier, wie im englischsprachigen Raum üblich, mit einem Punkt statt einem Komma geschrieben. Das Komma-Symbol wird nämlich zur Trennung von Eingabewerten verwendet. Die Mitte der Zeichenfläche befindet sich bei `(0, 0)`. Die x-Koordinate gibt an, wie weit rechts man sich von der Mitte befindet. Die y-Koordinate, wie weit oben.

+ `inge.setheading(45)`   
*"setze Blickrichtung"*  
Hiermit weiß Inge, in welche Richtung sie schauen soll. Die Angabe ist hier in Grad und entspricht der Richtung auf einem normalen Koordinatensystem. Hier ein paar wichtige Gradzahlen und die entsprechende Richtung:   
0 - rechts   
90 - oben   
180 - links   
270 - unten   
Eine kürzere Schreibweise ist `inge.seth(45)`

In der [englischsprachigen Dokumentation](https://docs.python.org/2/library/turtle.html) kann man sich alle anderen Befehle durchlesen, die Schildkröten außerdem noch können.


## Zusätzliche Befehle

+ `inge.shape("square")`   
*"Form"*  
Hiermit sieht Inge aus wie ein Quadrat. Folgende Werte kannst du verwenden: `"classic"`, `"arrow"`, `"circle"`, `"square"`, `"triangle"`, `"turtle"`

+ `inge.stamp()`   
*"Stempel"*  
Hinterlässt einen Abdruck von Inge auf der Malfläche.

+ `inge.width(4)`   
*"Breite"*  
Setzt die Strichbreite von Inge auf `4`.

+ `inge.backward(42)`   
*"rückwärts"*  
Inge geht `42` Pixel rückwärts, entgegengesetzt der aktuellen Blickrichtung.

+ `inge.color("red")`   
*"Farbe"*  
Farbe von Inge, und damit auch die Farbe der Linie, die Inge hinterlässt, auf rot ändern.   
*siehe Katalog für *[andere Farben](#katalog)*

+ `inge.fillcollor("blue")`   
*"Füllfarbe"*  
Farbe, mit der Formen ausgefüllt werden, auf blau ändern. Damit eine Form ausgefüllt wird, sollte davor `inge.begin_fill()` und danach `inge.end_fill()` stehen.

+ `inge.begin_fill()` und `inge.end_fill()`   
*"starte Füllung", "beende Füllung"*  
Damit startet oder beendet Inge, Formen mit Farbe zu füllen.

+ `inge.write("Mathe macht Spaß!", None, "center", "16pt bold")`   
*"schreibe"*  
Damit schreibt Inge den Text `"Mathe macht Spaß!"` an der Stelle, an der sie steht. Dabei ist die Mitte des Texts genau da, wo Inge gerade steht und der Text hat die Textgröße `16pt`.

+ `inge.hideturtle()`   
*"verstecke Schildkröte"*  
Lässt Inge verschwinden.

+ `inge.showturtle()`   
*"zeige Schildkröte"*  
Lässt Inge wieder auftauchen.



# Tipps und Tricks zur Programmieroberfläche

Mithilfe der Leiste direkt über dem Codebereich kannst du einige tolle Funktionen der Webseite trinket.io nutzen. Dazu gehört zum Beispiel die Möglichkeit, dir den Code an deine E-Mail-Adresse schicken zu lassen oder ihn herunterzuladen.


## Mehr Platz zum Programmieren

Auf deiner Tastatur findest du ziemlich weit unten links einen Knopf mit der Aufschrift `Strg` oder, falls du englische Tastaturbezeichnungen hast, `Ctrl`. Das steht für `Steuerung`, oder `Control`. Wenn du die `Strg`-Taste festhältst und dabei dein Mausrad nach oben oder unten drehst, kannst du die Seite größer oder kleiner machen. Dieser Trick funktioniert auch auf vielen anderen Webseiten.

Die Trennlinie zwischen der Codeeingabe und der Malfläche der Schildkröte kannst du mit einem Linksklick festhalten und seitlich verschieben.

Klickst du in der oberen Leiste auf das Symbol ganz links, erscheint eine Seitenleiste mit ein paar neuen Optionen. Mit Klick auf *"Fullscreen"* nehmen Codeeingabe und Malfläche den ganzen Bildschirm ein, sodass du noch mehr Platz hast.

*Tipp:* in dieser Seitenleiste sind noch weitere schöne Funktionen, wie die Anpassung der Schriftgröße oder deinen Code durch den anfänglichen Beispielcode zu ersetzen.


## Abkürzungen für Profis

* `Strg + Enter`  
**Code ausführen**  
Damit musst du nicht immer zur Maus greifen, um deinen Code zu testen.  
Die *Entertaste* — oder auch *Eingabetaste* oder *Return* — ist die etwas größere Taste direkt unter der Löschen-Taste, mit einem abgeknickten, nach links zeigenden Pfeil. Längere Tastaturen haben eine weitere solche Taste, die in den meisten Fällen genau gleich funktioniert, ganz ganz rechts unten, mit der Aufschrift *"Enter*".

* `Strg + Z`  
**Rückgängig: Letzte Änderung rückgängig machen.**  
*"Z" wie "Zurück".*  
Das ist die allerwichtigste Abkürzung fürs Programmieren. Wenn du aus Versehen zu viel gelöscht hast oder einfach zu einem Zustand von vor kurzem zurück willst, kannst du diese Abkürzung verwenden, sogar mehrmals hintereinander. Dafür reicht es, die Taste `Strg` festzuhalten und nur die Taste `Z` mehrmals zu drücken.

* `Strg + Y`  
**Wiederholen.**  
*"Y", weil es im Alphabet der Nachbar von "Z" ist.*  
Damit wird der letzte rückgängig gemachte Schritt wiederholt. Funktioniert nur, wenn gerade etwas rückgängig gemacht wurde.

* `Strg + C`  
**Kopieren: aktuell ausgewählten Text kopieren.**  
*"C" wie "Copy".*  
Das ist die zweitwichtigste Abkürzung fürs Programmieren. Damit kannst du es dir sparen, ausgewählten Text immer mit `Rechtsklick -> Kopieren` zu kopieren.  
Um Text auszuwählen, musst du an einem Ende des Textes deine Maus platzieren, dann die linke Maustaste festhalten und die Maus zum anderen Ende bewegen. Dort kannst du den Linksklick loslassen. Der ausgewählte Text wird durch eine farbige Markierung gekennzeichnet. Um eine Auswahl wieder zu entfernen, musst du nur den Cursor (das ist die kleine blinkende Linie, die dir anzeigt, wo du gerade schreiben kannst) bewegen, zum Beispiel indem du irgendwo anders hin klickst, oder die Pfeiltasten benutzt.

* `Strg + V`  
**Einfügen an aktueller Stelle**  
*"V", weil das direkt neben dem "C" fürs Kopieren ist.*  
Und damit haben wir die letzte der drei wichtigsten Abkürzungen fürs Programmieren. Damit kannst du es dir sparen, kopierten Text immer mit `Rechtsklick -> Einfügen` einzufügen.

* Pfeiltasten `← ↑ → ↓`  
**Cursor bewegen**  
Bewegt die Stelle, an der du gerade schreiben kannst. Damit kannst du dich flott im Code umher bewegen, ohne immer zur Maus greifen zu müssen. Probiere außerdem mal aus, dich mit den Pfeiltasten zu bewegen, während du die `Strg`-Taste festhältst. So kannst du ganze Wörter am Stück überspringen.  
Und wenn du die Umschalttaste (auch bekannt als Shift-Taste) — das ist die Taste mit dem dicken Pfeil nach oben, die man gedrückt halten muss, um Großbuchstaben zu schreiben — festhältst, dann wird der Text dabei ausgewählt.

## Verschiedene Dateien

Die Codeeingabe besteht anfangs tatsächlich nicht nur aus einer einzigen Textdatei, sondern aus zweien. Direkt über der Codeeingabe siehst du die Namen der Dateien: `main.py` und `shapes.py`. Mit Klick auf die Namen, kannst du zwischen ihnen wechseln. Mit dem **+**-Knopf rechts davon, kannst du weitere Dateien hinzufügen. Jede Datei, die nicht die ganz linkste ist, kannst du durch Klick auf das Zahnrad-Symbol löschen, welches neben dem Dateinamen auftaucht, wenn die Datei gerade angezeigt wird.

Klickst du auf den Play-Button in der oberen Leiste, wird immer der Code ausgeführt, der in der Datei `main.py` steht. Der Code aus den anderen Dateien kann aber in der `main.py` verwendet werden, indem wir ihn importieren. Das ist es, was beim Beispielcode die Zeile `from shapes import *` bewirkt. Sie importiert den ganzen Code aus der Datei `shapes.py`, wodurch du alle Manöver aus dieser anderen Datei auch in `main.py` benutzen kannst. Im Folgenden werden wir zwar alles nur in der Datei `main.py` programmieren, aber wenn du magst, kannst du deine Manöver in einer anderen Datei programmieren und sie dann in `main.py` aufrufen, wie es auch der Beispielcode macht.



# Allgemeine Python-Programmiertricks

Die folgenden Tipps und Tricks funktionieren allgemein bei Python, also auch ohne Schildkröte, und in leicht abgewandelter Form in fast jeder anderen Programmiersprache.


## Einfache Rechnungen

An jeder Stelle, an der eine Zahl stehen sollte, kann stattdessen auch eine Rechnung stehen, deren Ergebnis dann direkt eingesetzt wird. So kannst du zum Beispiel `inge.forward(20 + 30 - 5)` schreiben, damit Inge sich `45` vorwärts bewegt. Hier die Symbole, die du für Rechnungen brauchen könntest:

* Addition (Plus): `+`
* Subtraktion (Minus): `-`
* Multiplikation (Mal): `*`
* Division (Geteilt): `/`

Und hier noch zwei fortgeschrittenere Rechenoperationen:

* Exponentiation (Potenz): `**`
* Modulo (Rest nach Division): `%`


## Neue Manöver definieren mit `def`

Neben den Sachen, die unsere Schildkröte schon kann, können wir für unsere Schildkröte eine Art Anleitung für ein neues Manöver schreiben, die wir dann mit einer einzigen Zeile Code ausführen lassen können.

Lassen wir unsere Schildkröte Inge zum Beispiel einen kleinen Tanz aufführen. Hier ist der ganze Code dafür:
```python
from turtle import *

inge = Turtle()
inge.shape("turtle")
inge.speed(4)

inge.right(360)
inge.left(360)
inge.forward(100)
inge.left(420)
inge.backward(100)
inge.right(300)
inge.forward(100)
inge.right(200)
inge.left(120)
inge.right(400)
```



Um das als Anleitung eines Manövers festzuhalten, müssen wir dem Manöver nur einen Namen geben, zum Beispiel `tanz`, und die durchzuführenden Bewegungen dann eingerückt nach einer Zeile mit `def` am Anfang wie folgt aufschreiben (beachte die Hinweise direkt nach dem Code):
```python
from turtle import *

inge = Turtle()
inge.shape("turtle")
inge.speed(4)

def tanz():
  inge.right(360)
  inge.left(360)
  inge.forward(100)
  inge.left(420)
  inge.backward(100)
  inge.right(300)
  inge.forward(100)
  inge.right(200)
  inge.left(120)
  inge.right(400)
```

***Hinweis 1:*** Nach dem Manövernamen steht eine offene und dann eine geschlossene runde Klammer. Wofür wir die brauchen, sehen wir gleich.

***Hinweis 2:*** Nach den runden Klammern steht ein Doppelpunkt. Wir werden später noch weitere Anwendungen vom Doppelpunkt sehen. Die Zeile, in der am Ende ein Doppelpunkt steht, wirkt für alle kommenden "eingerückten" Zeilen. In diesem Fall wollen wir das Manöver `tanz` definieren und alle folgenden eingerückten Zeilen gehören zu diesem Manöver.

***Hinweis 3:*** *"Eingerückt"* heißt, dass wir vor jede der entsprechenden Zeilen einen zusätzlichen Leerraum einfügen, zum Beispiel zwei Leerzeichen. Wichtig ist nur, dass wir vor jeder Zeile die gleiche Anzahl an Leerzeichen setzen, wenn sie alle zum gleichen Manöver gehören sollen. Das Manöver geht bis zur letzten Zeile, die noch eingerückt ist.

Wenn wir diesen Code ausführen, passiert gar nichts. Siehst du vielleicht, warum?

Es passiert nichts, weil wir zwar jetzt eine Anleitung für ein wunderbares neues Manöver geschrieben haben, dieses Manöver aber nie ausführen lassen. Um es auszuführen, schreiben wir ganz einfach den Namen das Manövers und runde Klammern dahinter. Der ganze Code sieht dann so aus, mit einer neuen Zeile ganz am Ende:
```python
from turtle import *

inge = Turtle()
inge.shape("turtle")
inge.speed(4)

def tanz():
  inge.right(360)
  inge.left(360)
  inge.forward(100)
  inge.left(420)
  inge.backward(100)
  inge.right(300)
  inge.forward(100)
  inge.right(200)
  inge.left(120)
  inge.right(400)

tanz()
```

In die runden Klammern können wir Eingabewerte hineinschreiben, um ein bisschen Variation in unser Manöver reinzubekommen. Dafür müssen wir zuerst diese neuen Eingabewerte in der Anleitung in die runden Klammern schreiben, und idealerweise auch in der Anleitung verwenden. Und dann können wir an der Stelle, an der wir das Manöver ausführen wollen, passende Eingabewerte eingeben. Es kommt dabei auf die Reihenfolge an, in der die Werte in der Anleitung in den runden Klammern stehen.

So können wir als Eingabewerte zum Beispiel die Geschwindigkeit und die Strichlänge beim Tanz mitgeben. Die Eingabeparameter können wir, wie auch den Namen der Schildkröte und den Namen des Manövers, nach den [oben](#benennung-der-schildkröte-und-sonstiger-variablen) beschriebenen Regeln wählen. Auch innerhalb eines Manövers werden, wie sonst überall auch, leere Zeilen ignoriert:
```python
from turtle import *

inge = Turtle()
inge.shape("turtle")
inge.speed(4)

def tanz(schnelligkeit, distanz):
  inge.speed(schnelligkeit)

  inge.right(360)
  inge.left(360)
  inge.forward(distanz)

  inge.left(420)
  inge.backward(distanz)

  inge.right(300)
  inge.forward(distanz)

  inge.right(200)
  inge.left(120)
  inge.right(400)

tanz(7, 80)
```

Sogar die Schildkröte selbst kann als Eingabewert mitgegeben werden. In diesem Beispiel haben wir zwei Schildkröten, die nacheinander den Tanz ausführen, beide in einer etwas anderen Art:
```python
from turtle import *

inge = Turtle()
inge.shape("turtle")

svenja = Turtle()
svenja.color("orange")

def tanz(name, schnelligkeit, distanz):
  name.speed(schnelligkeit)

  name.right(360)
  name.left(360)
  name.forward(distanz)

  name.left(420)
  name.backward(distanz)

  name.right(300)
  name.forward(distanz)

  name.right(200)
  name.left(120)
  name.right(400)

svenja.left(90)
svenja.forward(40)

tanz(inge, 7, 80)

tanz(svenja, 3, 100)
```

**Bonus:** In der Fachsprache sagt statt "Manöver" oft "Funktion" oder manchmal auch "Methode".

## Wiederholungen: Schleifen mit `for` und `while`

Im Folgenden wollen wir Anhand von Beispielen lernen, wie wir dem Computer erklären, dass wir etwas wiederholt machen wollen. Dadurch wird unser Code nicht nur **kürzer und leichter zu bearbeiten**, sondern auch **weniger fehleranfällig**.


### `for`-Schleife
```python
from turtle import *

inge = Turtle()
inge.shape("turtle")

for i in range(5):
  inge.forward(40)
  inge.left(30)
```

Probiere den Code mal aus und schau, was dabei herauskommt! Kannst du dir erklären, warum das passiert?

Hier ist noch ein Beispiel:
```python
strecke = 5
for zaehler in range(70):
  inge.forward(strecke)
  inge.left(91)
  strecke = strecke + 3
```

Zuerst erstellen wir hier die Variable `strecke` mit dem Anfangswert `5`. Das brauchen wir nicht konkret für Schleifen, aber in der dann folgenden Schleife möchten wir diese Variable verwenden.   

Danach kommt auch schon der wichtigste Teil der Schleife: die Zeile, die mit `for` beginnt. Diese lässt sich so lesen: *"Für jede Zahl im Bereich von 0 bis 70 (letzte Zahl wird ausgeschlossen), führe folgenden Code aus"*.

Dafür schreiben wir `for`, gefolgt von einer Variable, die von `0` bis `69` durchgezählt wird. Sie beginnt bei `0` und geht bis `69`, weil `range(70)` uns alle Zahlen bis `70` gibt, angefangen bei `0` und bis eine Zahl vor `70`. `range` ist deswegen super praktisch für `for`-Schleifen, weil wir so das, was in unserer Schleife passieren soll, genau 70 mal machen können. Unsere Variable `zaehler` wird nämlich nach jedem Schleifendurchlauf genau um `1` erhöht, bis wir `70` erreicht haben.

Nach einem Doppelpunkt folgt in den nächsten Zeilen das, was in unserem Fall `70` mal passieren soll. Das alles müssen wir aber gleich weit nach rechts einrücken, zum Beispiel durch ein Leerzeichen zu Beginn der Zeile. Denn alles, was genau so weit links steht, wie `for`, gehört nicht mehr zur Schleife. Das wird also erst nach der Schleife ausgeführt.

In unserer Schleife geht Inge immer `strecke` weit vorwärts und dreht sich dann um `91` Grad nach links. Also fast im rechten Winkel, nur ein bisschen weiter noch. Danach wird die Variable `strecke` um `3` erhöht, wodurch Inge beim nächsten Schleifendurchlauf `3` weiter laufen wird, als beim letzten Mal.

*Bonus*: Wir können obigen Code auch folgendermaßen schreiben, mit dem gleichen Ergebnis. Überlege dir, warum:
```python
for i in range(70):
  inge.forward((i * 3) + 5)
  inge.left(91)
```

### `while`-Schleife

Diese braucht man, um etwas so lange auszuführen, wie eine Aussage wahr ist. Das heißt, die Schleife bricht ab, sobald die Aussage nicht mehr wahr ist. Ein lustiges Thema, brauchen wir aber nicht für Fraktale. Trotzdem schnell ein Beispiel:
```python
strecke = 1
maxstrecke = 150
while strecke < maxstrecke:
  inge.forward(strecke)
  inge.right(60)
  strecke = strecke + 3
```

Hier starten wir mit einer Variable `strecke` mit dem Wert `1` und einer Variable `maxstrecke` mit dem Wert `150`. In der Schleifenvorschrift schreiben wir *while* gefolgt von einer Aussage, die entweder wahr, oder falsch ist. Solange `strecke` kleiner als `maxstrecke` ist, wird also die Schleife weitergehen. Da aber am Ende jedes Schleifendurchlaufs der Wert von `strecke` um `3` erhöht wird, stimmt diese Aussage irgendwann nicht mehr und die Schleife bricht ab.

So eine Schleife kannst du auch ganz einfach unbegrenzt lange ausführen lassen. Das Programm endet dann erst, wenn du selbst in der oberen Leiste auf den Stop-Knopf drückst. Das geht ganz einfach dann, wenn die `while`-Bedingung immer wahr bleibt, zum Beispiel indem wir direkt den Wahrheitswert `True` hineinschreiben — englisch für "wahr":
```python
strecke = 1
while True:
  inge.forward(strecke)
  inge.right(60)
  strecke = strecke + 3
```

*Tipp:* Das Gegenteil von `True` ist `False`.

*Bonus:* `while`- sowie auch `for`-Schleifen können vorzeitig abgebrochen werden mit dem `break`-Schlüsselwort. Wir können so das erste Beispiel einer `while`-Schleife auf diese Weise nachmachen:
```python
strecke = 1
maxstrecke = 150
while True:
  inge.forward(strecke)
  inge.right(60)
  strecke = strecke + 3
  if strecke >= maxstrecke:
    break
```

Beachte, dass dieses Beispiel hier eher unschön ist, weil man die Bedingung in diesem Fall lieber einfach in die `while`-Bedingung schreiben würde. Das `break`-Schlüsselwort wird erst bei komplizierteren Schleifen wirklich praktisch.



## Bedingte Code-Ausführung mit `if` und `else`

Vor allem wenn wir Schleifen oder Manöver haben, wollen wir manchmal Code nur dann ausführen, wenn eine bestimmte Bedingung erfüllt ist. Und wenn die Bedingung nicht erfüllt ist, wollen wir entweder gar nichts machen, oder stattdessen anderen Code. Dafür brauchen wir die Schlüsselwörter `if` und `else` — englisch für "wenn" und "sonst".

Hier ist ein Beispiel, bei dem wir in einer Schleife die Farbe von Inge anpassen, sobald die Schleife weit genug vorangeschritten ist:

```python
from turtle import *

inge = Turtle()
inge.shape("turtle")
inge.speed(0)
inge.width(3)

for i in range(60):
  if i == 35:
    inge.color("magenta")
  inge.forward(i * 4)
  inge.left(90)
```

Durch die Zeile `inge.width(3)` werden die Strichfarben besser sichtbar.

Jetzt wollen wir versuchen, nur jeden vierten Strich magenta zu machen. Das heißt, in jedem vierten Schleifendurchlauf soll Inge magenta sein. Das kriegen wir ganz leicht hin, wenn wir einfach immer abfragen, ob der Schleifenzähler `i` geteilt durch `4` einen bestimmten Rest hat. Im Folgenden Beispiel überprüfen wir, ob der Rest gleich `2` ist (das ist der Fall für `i` gleich 2, 6, 10, 14, 18, ...). Das wird leider noch nicht ganz so funktionieren, wie wir das möchten. Siehst du, warum?:
```python
from turtle import *

inge = Turtle()
inge.shape("turtle")
inge.speed(0)
inge.width(3)

for i in range(60):
  if (i % 4) == 2:
    inge.color("magenta")

  inge.forward(i * 4)
  inge.left(90)
```

Wie du vielleicht nach einem Testversuch gesehen hast, wird Inge direkt nach zwei Schritten magenta und bleibt dann so, anstatt wieder schwarz zu malen. Deswegen müssen wir sie beim nächsten Strich wieder schwarz machen. Natürlich könnten wir sie immer genau dann schwarz machen, wenn der Rest gleich `3` ist — also eins nach `2` — aber einfacher ist es, sie einfach in jedem anderen Schritt schwarz zu machen. Das heißt **wenn** der Rest durch `2` teilbar ist, machen wir sie magenta, und **sonst** soll sie schwarz sein. Dafür verwenden wir das Schlüsselwort `else`. Probiere doch auch mal andere Zahlen in diesem Code aus, als `2` für den Rest (Achtung: es gehen nur alle Zahlen von `0` bis eins vor die Zahl, durch die wir teilen, da das die einzigen möglichen Reste sind, also hier bis `3`):
```python
from turtle import *

inge = Turtle()
inge.shape("turtle")
inge.speed(0)
inge.width(3)

for i in range(60):
  if (i % 4) == 2:
    inge.color("magenta")
  else:
    inge.color("black")

  inge.forward(i * 4)
  inge.left(90)
```

Für unser letztes Beispiel sehen wir noch eine Mischung von `if` und `else`, und zwar `elif`. In vielen anderen Programmiersprachen wird dafür einfach `else if` geschrieben. Genau wie `else`, kann `elif` nur direkt nach `if` oder nach einem `elif` auftauchen. Der Zweck ist folgender: *"**Wenn** A, mache a, **sonst wenn** B, mache b, **sonst** mache c"*. In diesem Satz sind "A" und "B" jeweils Bedingungen, und "a", "b" und "c" steht für Code, der in diesem Fall ausgeführt wird.

Wir programmieren damit wieder einen Farbwechsel.

Beim vorherigen Beispiel ersetzen wir die Farbe `"magenta"` durch `"orange"` und möchten zusätzlich zu den bisherigen Regeln, dass Inge bei jedem dritten Strich blau wird. Aber wenn wir bei einem Durchlauf sind, bei dem sie orange sein sollte, dann soll sie orange sein, obwohl es auch ein dritter Druchlauf ist. So sieht der Code vielversprechend aus, aber hier stimmt etwas noch nicht. Probiere ihn aus:
```python
from turtle import *

inge = Turtle()
inge.shape("turtle")
inge.speed(0)
inge.width(3)

for i in range(60):
  if (i % 4) == 2:
    inge.color("orange")
  else:
    inge.color("black")

  if (i % 3) == 1:
    inge.color("blue")

  inge.forward(i * 4)
  inge.left(90)
```

So werden Striche, die eigentlich orange sein sollte, nun doch blau angemalt. Wie lösen wir dieses Problem? Mit `elif` wird es ganz einfach: zuerst wollen wir sicherstellen, dass jeder vierte Strich orange wird. **Sonst wenn** es ein dritter Durchlauf ist, dann wird der Strich blau. Und ansonsten wird er schwarz:

```python
from turtle import *

inge = Turtle()
inge.shape("turtle")
inge.speed(0)
inge.width(3)

for i in range(60):
  if (i % 4) == 2:
    inge.color("orange")
  elif (i % 3) == 1:
    inge.color("blue")
  else:
    inge.color("black")

  inge.forward(i * 4)
  inge.left(90)
```


### Vergleichen

Oft wollen wir zum Beispiel wissen, ob zwei Variablen gleich sind, oder ob eine Zahl größer ist, als eine andere. Dafür können wir in Python unter anderem folgendes nutzen in einer `if`-Abfrage oder auch in einer `while`-Bedingung:

* `a == b`  
a gleich b

* `a != b`  
a nicht gleich b

* `a > b`  
a größer als b

* `a >= b`  
a größer oder gleich b

* `a < b`  
a kleiner als b

* `a <= b`  
a kleiner oder gleich b



# Übungsaufgaben und Beispiele

## Quadrate malen

Versuche, diesen Code zu verstehen und auszuführen:
```python
inge.forward(100)
inge.right(90)

inge.forward(100)
inge.right(90)

inge.forward(100)
inge.right(90)

inge.forward(100)
inge.right(90)
```

Mithilfe einer Schleife können wir diesen Code stark vereinfachen:
```python
for i in range(4):
  inge.forward(100)
  inge.right(90)
```

Und wenn wir auf die schnelle ein Quadrat malen können wollen, können wir das auch als neues Manöver verpacken und zum Beispiel gleich danach auch ausführen:
```python
def quadrat():
  for i in range(4):
    inge.forward(100)
    inge.right(90)

quadrat()
```

Und um flexibler zu sein damit, welche Schildkröte das Manöver ausführt und wie weit sie läuft, können wir auch diese zwei Infos im Manöver mitgeben:
```python
def quadrat(name, distanz):
  for i in range(4):
    name.forward(distanz)
    name.right(90)

quadrat(inge, 100)
```

Hier noch der volle Code, in dem vier verschieden geformte und gefärbte Schildkröten verschiedene Quadrate malen.
```python
from turtle import *

# Wir erstellen vier verschiedene Schildkröten, mit verschiedenen Formen und Farben:
tau = Turtle()
tau.shape("square")
tau.color("lime")
svenja = Turtle()
svenja.shape("circle")
svenja.color("orange")
kathrin = Turtle()
kathrin.shape("triangle")
kathrin.color("red")
inge = Turtle()
inge.shape("turtle")
inge.color("blue")

def quadrat(name, distanz):
  for i in range(4):
    name.forward(distanz)
    name.right(90)

quadrat(inge, 100)
quadrat(svenja, 123)
quadrat(kathrin, 55)
quadrat(tau, 31.4159)
```



## Ein gleichseitiges Dreieck malen

Versuche, ein gleichseitiges Dreieck zu malen.

*Bonusaufgabe:* definiere es als neues Manöver, das zwei Eingabewerte nimmt. Der erste ist der Name der Schildkröte, die das Dreieck malen soll. Der zweite ist die Seitenlänge des Dreiecks.

## Ein Hexagon malen

Versuche, ein gleichmäßiges Sechseck zu malen.

## Ein Oktagon malen

Versuche, diesen Code zu reparieren, sodass ein gleichmäßiges Achteck gemalt wird:
```python
for i in range(6):
  inge.forward(70)
  inge.right(45)
```

## Quadratspirale

Versuche, diesen Code zu verstehen und auszuführen:
```python
from turtle import *

inge = Turtle()
inge.shape("turtle")
inge.speed(0)

for i in range(70):
  inge.forward(i * 3)
  inge.left(91)
```

Probiere folgende Variationen und auch eigene aus:
```python
from turtle import *

inge = Turtle()
inge.shape("turtle")
inge.speed(0)

for i in range(150):
  inge.forward(i)
  inge.left(61)
```


## Stempeln und Wechseln

Versuche, diesen Code zu verstehen und auszuführen:
```python
from turtle import *

inge = Turtle()
inge.shape("turtle")
inge.speed(3)

for i in range(6):
  inge.forward(100)
  inge.right(60)

  inge.stamp()

  if i % 2 == 0:
    inge.shape("circle")
  else:
    inge.shape("triangle")
```

*Tipp:* In der `if`-Bedingung wird abgefragt, ob die aktuelle Schleifenzahl `i` geteilt durch `2` einen Rest von `0` hat. Das ist gleichbedeutend damit, dass die Zahl `i` gerade ist, wird also bei jedem zweiten Durchlauf "wahr" sein, sonst "falsch".



# Katalog
Hier findest du mögliche Eingaben für oben beispielhaft verwendete Werte, wie mögliche Farben in Python Turtle, mögliche Richtungsangaben und alles weitere.   

## Formen für `.shape(...)`
Folgende Werte kannst du verwenden: `"classic"`, `"arrow"`, `"circle"`, `"square"`, `"triangle"`, `"turtle"`

## Farben für `.color(...)` 
`"black"`, `"green"`, `"blue"`, `"yellow"`, `"red"`, und viele andere englische Farbbezeichnungen funktionieren. [Hier](https://trinket.io/docs/colors) siehst du alle Farben, die bei Python Turtle auf [trinket.io](https://trinket.io) funktionieren. Wenn du auf eine Farbe draufklickst, siehst du unter anderem ihren Namen und ihren RGB-Wert.
Du kannst auch direkt die [RGB-Werte](https://de.wikipedia.org/wiki/RGB-Farbraum) deiner gewünschten Farbe eingeben, wie zum Beispiel `inge.color((65, 105, 225))` für ein sattes RoyalBlue. In allen Fällen zum Beispiel, in denen du ein kräftiges Rot haben willst, kannst du statt `"red"` einfach `(255, 0, 0)` eingeben.   
Eine schöne Tabelle mit den RGB-Werten zu ganz vielen Farben gibt es [Hier](http://www.farb-tabelle.de/de/farbtabelle.htm).

### Farbe zufällig ändern

Hiermit kannst du die Farbe von Inge zufällig ändern. Du musst dafür aber ganz oben noch das Modul `random` importieren. Dafür schreibst du am einfachsten direkt in die erste Zeile `import random`:
```python
a = random.randint(1, 255)
b = random.randint(1, 255)
c = random.randint(1, 255)
inge.color((a, b, c))
```

Und hier wird bei einer Pentagon-Spirale die Farbe zufällig geändert:
```python
from turtle import *
import random

inge = Turtle()
inge.shape("turtle")
inge.speed(0) # höchste Geschwindigkeit
inge.width(5) # breitere Striche

for i in range(130):
  # Farbwerte zufällig wählen
  a = random.randint(1, 255)
  b = random.randint(1, 255)
  c = random.randint(1, 255)

  # Farbe von inge ändern
  inge.color((a, b, c))

  # einen Schritt für die Pentagonspirale machen
  inge.forward(i*2)
  inge.left(72)
```



## Grad für Blick- und Drehrichtung:
`inge.right(90)` dreht Inge genau im rechten Winkel nach rechts. Wie wir wissen, steht 90 Grad nämlich für den rechten Winkel. Eine ganze Drehung entspricht 360 Grad. Wenn wir Inge nach hinten umdrehen wollen, wäre mit `inge.left(60)` genau ein Drittel der Drehung geschafft, stattdessen mit `inge.left(180)` sogar die ganze Drehung nach hinten.

`inge.seth(0)` lässt Inge nach rechts schauen, denn 0 Grad ist im Koordinatensystem genau rechts. 90 Grad ist oben, 180 Grad ist links, 270 Grad ist unten und 360 Grad ist wieder rechts und das gleiche wie 0 Grad - zumindest wenn man von Grad als Rotationsangabe spricht. Entsprechend schaut Inge mit `inge.seth(135)` nach oben links.


