# Sierpinski-Dreieck

Informationen zum Sierpinski-Dreieck gibt es [Hier](https://de.wikipedia.org/wiki/Sierpinski-Dreieck).   

![Sierpinski-Dreieck 0. Iteration (Dreieck)](../images/sierpinski-png/sierpinski-0.png "Sierpinski It. 0") ![Sierpinski-Dreieck 1. Iteration](../images/sierpinski-png/sierpinski-1.png "Sierpinski It. 1") ![Sierpinski-Dreieck 2. Iteration](../images/sierpinski-png/sierpinski-2.png "Sierpinski It. 2") ![Sierpinski-Dreieck 3. Iteration](../images/sierpinski-png/sierpinski-3.png "Sierpinski It. 3")   

Ganz wichtig: Immer, wenn wir ein neues Manöver in `shapes.py` programmiert haben, wollen wir dieses ausprobieren. Das hilft uns zu sehen, ob wir Fehler gemacht haben und ob das gewünschte Ergebnis herauskommt. Ab hier wird nicht mehr extra daran erinnert. Trotzdem werden wir jedes neu definierte Manöver in unserer `main.py` testen!

In der ersten Iteration nur ein Dreieck:
```python
def sierpinski(turtle, laenge):
  turtle.pendown()

  turtle.forward(laenge)
  turtle.left(120)
  turtle.forward(laenge)
  turtle.left(120)
  turtle.forward(laenge)
  turtle.left(120)

  turtle.penup()
```

Da wir viele Dreiecke malen werden, erstellen wir ein neues Manöver `dreieck`. Hier gleich mit Schleife, damit wir nicht 3 mal die gleichen 2 Zeilen Code schreiben müssen:
```python
def dreieck(turtle, laenge):
  for i in range(3):
    turtle.forward(laenge)
    turtle.left(120)
```

Jetzt sieht `sierpinski` schon schöner aus:
```python
def sierpinski(turtle, laenge):
  turtle.pendown()
  
  dreieck(turtle, laenge)
  
  turtle.penup()
```

Nun wollen wir, dass unser Dreieck aus 3 kleineren Dreiecken besteht. Die Seitenlänge dieser kleineren Dreiecke ist genau halb so lang, wie vom großen. Wir könnten nun, um einen Iterationsschritt mehr zu bekommen, also 3 kleinere Dreiecke malen. Wir merken aber auch, dass wir jetzt immer noch unsere Schildkröte erst in die richtige Position bringen müssen!
```python
def sierpinski(turtle, laenge):
  turtle.pendown()
  
  dreieck(turtle, laenge/2)
  turtle.forward(laenge/2)
  
  dreieck(turtle, laenge/2)
  turtle.forward(laenge/2)
  turtle.left(120)
  turtle.forward(laenge/2)
  
  dreieck(turtle, laenge/2)
  turtle.forward(laenge/2)
  turtle.left(120)
  turtle.forward(laenge/2)
  
  turtle.forward(laenge/2)
  turtle.left(120)

  turtle.penup()
```

Als nächstes bringen wir Rekursion ins Spiel. Wir rufen einfach statt `dreieck` wieder `sierpinski` in sich selbst auf, um rekursiv immer kleinere Dreiecke zu malen (**Achtung**, funktioniert so noch nicht ganz! Siehe dafür den nächsten Text und Code):
```python
def sierpinski(turtle, laenge):
  turtle.pendown()
  
  sierpinski(turtle, laenge/2)
  turtle.forward(laenge/2)
  
  sierpinski(turtle, laenge/2)
  turtle.forward(laenge/2)
  turtle.left(120)
  turtle.forward(laenge/2)
  
  sierpinski(turtle, laenge/2)
  turtle.forward(laenge/2)
  turtle.left(120)
  turtle.forward(laenge/2)
  
  turtle.forward(laenge/2)
  turtle.left(120)
  
  turtle.penup()
```

Damit wir das aber nicht bis in alle Ewigkeit machen (das ist sowieso nicht möglich!), bestimmen wir ein unteres Limit `limitmin`. Ist dieses erreicht, läuft unsere Schildkröte einfach wieder ein Dreieck ab, anstatt nochmal `sierpinski` aufzurufen:
```python
def sierpinski(turtle, laenge, limitmin):
  turtle.pendown()
  
  if laenge < limitmin*2:
    #turtle.begin_fill()
    dreieck(turtle, laenge)
    #turtle.end_fill()
  
  else:
    sierpinski(turtle, laenge/2, limitmin)
    turtle.forward(laenge/2)
    
    sierpinski(turtle, laenge/2, limitmin)
    turtle.forward(laenge/2)
    turtle.left(120)
    turtle.forward(laenge/2)
  
    sierpinski(turtle, laenge/2, limitmin)
    turtle.forward(laenge/2)
    turtle.left(120)
    turtle.forward(laenge/2)
  
    turtle.forward(laenge/2)
    turtle.left(120)
  
  turtle.penup()
```

Um jetzt noch die kleinsten Dreiecke auszufüllen, wie wir es normalerweise vom Sierpinski-Dreieck kennen, müssen wir nur im Code hierüber bei den Kommentaren die `#` entfernen, also direkt vor und nach `dreieck(turtle, laenge)`.


## Unser Beispielcode sieht am Ende so aus: 

`main.py`:
```python
from turtle import *
from shapes import *

inge = Turtle()
inge.shape("turtle")
inge.speed(70)

inge.color("black")
inge.fillcolor("black")

inge.penup()
inge.goto(-100, -80)

sierpinski(inge, 200, 25)

inge.goto(0, -110)
inge.write("Mein Sierpinski-Dreieck :-)", None, "center", "15pt bold")
inge.goto(0, -125)
inge.seth(0)
```


`shapes.py`:
```python
import turtle

def sierpinski(turtle, laenge, limitmin):
  turtle.pendown()
  
  if laenge < limitmin*2:
    dreieck(turtle, laenge)
  
  else:
    sierpinski(turtle, laenge/2, limitmin)
    turtle.forward(laenge/2)
    
    sierpinski(turtle, laenge/2, limitmin)
    turtle.forward(laenge/2)
    turtle.left(120)
    turtle.forward(laenge/2)
  
    sierpinski(turtle, laenge/2, limitmin)
    turtle.forward(laenge/2)
    turtle.left(120)
    turtle.forward(laenge/2)
  
    turtle.forward(laenge/2)
    turtle.left(120)
  
  turtle.penup()

def dreieck(turtle, laenge):
  for i in range(3):
    turtle.forward(laenge)
    turtle.left(120)
```


## So könnte `sierpinski` aussehen mit Iterationsanzahl `schritte` als Eingabeparameter, statt mindester Strichlänge `limitmin`
```python
def sierpinski(turtle, laenge, schritte):
  turtle.pendown()
  
  if schritte < 1:
    dreieck(turtle, laenge)
  
  else:
    sierpinski(turtle, laenge/2, schritte-1)
    turtle.forward(laenge/2)
    
    sierpinski(turtle, laenge/2, schritte-1)
    turtle.forward(laenge/2)
    turtle.left(120)
    turtle.forward(laenge/2)
  
    sierpinski(turtle, laenge/2, schritte-1)
    turtle.forward(laenge/2)
    turtle.left(120)
    turtle.forward(laenge/2)
  
    turtle.forward(laenge/2)
    turtle.left(120)
  
  turtle.penup()
```
Wie man sieht, fast genau so wie vorher! Geändert wurde tatsächlich nur genau jede Stelle, an der vorher das Wort `limitmin` stand und die Bedingung bei der *if*-Abfrage. Der Rest des Codes ist wie zuvor.


# Bonusaufgaben

Du hast die Anleitung zum Sierpinski-Dreieck verstanden oder das ganze Dreieck sogar erfolgreich selbst programmiert? Dann versuche dich an diesen Bonusaufgaben! Ändere deinen Code so, dass du solche oder ähnliche Dreiecke erzeugen kannst. Zur Hilfe wird die Idee hinter jedem Bild beschrieben.   

Falls du selbst auf interessante Variationen des Sierpinski-Dreiecks kommst, kannst du mir sehr gerne dein Ergebnis unter alex.mai@posteo.net zuschicken. Vielleicht schafft es deine Variation ja in diese Bonusaufgabensammlung! Und falls du meine Lösungsvorschläge zu den Bonusaufgaben sehen magst, zum Beispiel um den Fehler in deinem Programm zu finden, kannst du mich auch unter alex.mai@posteo.net anschreiben.


### Dreieck farbig und ausgefüllt ("Farb-Sierpinski")
Die nach oben zeigenden Dreiecke sollen mit einer gewählten Farbe ausgefüllt, die Ränder mit einer anderen Farbe gemalt werden.   
![Sierpinski-Dreieck orangener Rand und gelbe Füllung](../images/sierpinski-png/sierpinski-farbig-ausgefuellt.png "Sierpinski farbig ausgefüllt")   


### Dreieck mit zufälligen Farben bei jedem kleinsten Dreieck ("Regenbogen-Sierpinski")
Hier soll einfach jedes kleinste, nach oben zeigende Dreieck in einer zufälligen Farbe gefüllt und umrahmt werden. Eine Vorlage zur Erzeugung von zufälligen Farben für Inge findest du in der [einführenden Python Turtle Anleitung, ganz unten bei "Lustige Beispiele"](/Malen-in-Python-Turtle/README.md/#lustige-beispiele).   
![Sierpinski-Dreieck mit zufälligen Farben bei jedem Dreieck, 3 It.](../images/sierpinski-png/sierpinski-bunt.png "Sierpinski bunt")   


### Dreieck mit verschiedenen Winkeln ("Winkel-Sierpinski")
Hier soll der Winkel der oberen Dreieckswinkel angepasst werden können. Das kann man schön als Eingabeparameter übergeben. Da das recht schwer ist (ich musste auf schöne Trigonometrie zurückgreifen!), kannst du zunächst versuchen, es für einen bestimmten Winkel zu programmieren, wie für 90 Grad.
![Sierpinski-Dreieck flacher](../images/sierpinski-png/sierpinski-flacher.png "Sierpinski flacher") ![Sierpinski-Dreieck spitzer](../images/sierpinski-png/sierpinski-spitzer.png "Sierpinski spitzer")   


### "Quadrat-Sierpinski", "Raute-Sierpinski", "Perspektiven-Sierpinski"
Bei den kleinsten, nach oben zeigenden Dreiecken wollen wir hier jeweils ein Quadrat auf den unteren Dreiecksstrich setzen (erstes Bild). Dann wollen wir den unteren Strich des Dreiecks entfernen (zweites Bild). Dann vielleicht noch aus den Quadraten noch Rauten machen (drittes Bild). Schließlich noch einen Strich zwischen oberer Rautenecke und oberer Dreiecksecke, und ein wenig Farbe (viertes Bild).   
![Sierpinski-Dreieck mit Quadraten unten](../images/sierpinski-png/sierpinski-3-quadrat.png "Sierpinski mit Quadraten") ![Sierpinski-Dreieck mit leeren Quadraten unten](../images/sierpinski-png/sierpinski-3-quadratDeckend.png "Sierpinski mit deckenden Quadraten") ![Sierpinski-Dreieck mit leeren Rauten unten](../images/sierpinski-png/sierpinski-3-rauteDeckend.png "Sierpinski mit deckenden Rauten") ![Perspektivisch cooles Sierpinski-Dreieck](../images/sierpinski-png/sierpinski-3-rautePerspektivisch.png "Sierpinski mit Perspektive")   

