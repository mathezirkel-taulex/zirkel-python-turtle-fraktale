# zirkel-python-turtle-fraktale

Eine [ausführliche Einleitung (hier klicken)](./tutorials/README.md) zu Python Turtle, darin auch Anleitungen für Fraktale.

Hier kannst du [im Menger-Schwamm herumfliegen](http://hirnsohle.de/test/fractalLab/).

Verlagerung eines alten Repositorys, das irgendwann mal gelöscht wird: [https://gitlab.com/CptMaister/spass-mit-fraktalen](https://gitlab.com/CptMaister/spass-mit-fraktalen).